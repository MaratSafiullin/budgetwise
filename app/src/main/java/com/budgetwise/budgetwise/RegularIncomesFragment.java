package com.budgetwise.budgetwise;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.RegularIncomeRec;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Fragment used to manage regular income sources (view list, add, edit, delete)
 */
public class RegularIncomesFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;
    //possible says of week to set
    private String[] weekDays;
    //possible says of month to set
    private String[] monthDays;

    /**
     * Async task to refresh the list of sources
     */
    protected class RegularIncomesListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            RegularIncomesArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of all sources from DB, pass it to the on post function
         * @param objects Parameters: period start(string), period end(string), link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            ArrayList<RegularIncomeRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getRegularIncomesList();
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Regular Income Sources", Toast.LENGTH_LONG).show();
                    }
                });
            }
            RegularIncomesArrayAdapter adapter = new RegularIncomesArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to change regular income sources records (add, edit, delete)
     */
    protected class RegularIncomesChanger extends AsyncTask{
        //possible actions
        protected static final int ADD = 0, UPDATE = 1, DELETE = 2;

        /**
         * On post execute refresh sources list
         * @param o List view to refresh
         */
        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                refreshRegularIncomeList((ListView) o);
            }
            activity.fillAutomaticIncomes();
        }


        /**
         * Main function. Perform changing action
         * @param objects Parameters: action type, id, name,  monthly/weekly, day number, start date, amount, link to list view
         * @return List view to refresh
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            int actionType = (Integer) objects[0];
            int id = 0;
            if (objects[1] != null) id = (Integer) objects[1];
            String name = "";
            if (objects[2] != null) name = (String) objects[2];
            boolean isMonthly = false;
            if (objects[3] != null) isMonthly = (Boolean) objects[3];
            int dayNum = 0;
            if (objects[4] != null) dayNum = (Integer) objects[4];
            String startDate = "";
            if (objects[5] != null) startDate = (String) objects[5];
            BigDecimal amount = new BigDecimal("0");
            if (objects[6] != null) amount = (BigDecimal) objects[6];

            try {
                switch (actionType){
                    case ADD:
                        databaseHelper.addRegularIncome(name, isMonthly, dayNum, startDate, amount);
                        break;
                    case UPDATE:
                        databaseHelper.updateRegularIncome(id, name, isMonthly, dayNum, startDate, amount);
                        break;
                    case DELETE:
                        databaseHelper.deleteRegularIncome(id);
                        break;
                }
            } catch (Exception e) {
                final String message;
                switch (actionType){
                    case ADD:
                        message = "Error! Can't add a Regular Income Source";
                        break;
                    case UPDATE:
                        message = "Error! Can't edit a Regular Income Source";
                        break;
                    case DELETE:
                        message = "Error! Can't delete a Regular Income Source";
                        break;
                    default:
                        message = "Error! Unknown Regular Income Source operation";
                        break;
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            return objects[7];
        }
    }

    /**
     * Adapter to fill sources list view with rows
     */
    protected class RegularIncomesArrayAdapter extends ArrayAdapter<RegularIncomeRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<RegularIncomeRec> values;

        /**
         * Listener for delete button click
         */
        protected final View.OnClickListener btnDelRegularIncomeClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.regularIncomeOperationFlag = MainActivity.BudgetWiseAppState.REGULARINCOME_DELETE;
                ListView listView = (ListView) activity.findViewById(R.id.listViewRegularIncomes);
                int position = listView.getPositionForView(v);
                showRegularIncomeEditBox((RegularIncomesArrayAdapter) listView.getAdapter(), position);
            }
        };

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public RegularIncomesArrayAdapter(Context context, ArrayList<RegularIncomeRec> values) {
            super(context, R.layout.row_layout_regular_income, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_categories" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_regular_income, parent, false);
            TextView textViewRegIncomeName = (TextView) rowView.findViewById(R.id.textViewRegularIncomeName);
            TextView textViewRegIncomeAmount = (TextView) rowView.findViewById(R.id.textViewRegularIncomeAmount);
            //set text
            textViewRegIncomeName.setText(values.get(position).name);
            textViewRegIncomeAmount.setText(values.get(position).amount.toString());
            //assign del button click listener
            ImageButton btnDelRegularIncome = (ImageButton) rowView.findViewById(R.id.imageButtonDeleteRegularIncome);
            btnDelRegularIncome.setOnClickListener(btnDelRegularIncomeClick);

            return rowView;
        }
    }

    /**
     * Listener for sources list item clicked
     */
    protected class RegularIncomeItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Show regular income record edit box, pass parameters
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            RegularIncomesArrayAdapter adapter = (RegularIncomesArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.regularIncomeOperationFlag = MainActivity.BudgetWiseAppState.REGULARINCOME_EDIT;
            showRegularIncomeEditBox(adapter, position);
        }
    }

    /**
     * Adapter to fill days list
     */
    protected class DaysArrayAdapter extends ArrayAdapter<String> {
        //Context
        protected final Context context;
        //List of values
        protected final String[] values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public DaysArrayAdapter(Context context, String[] values) {
            super(context, R.layout.row_layout_simplespinner, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            TextView textViewName = (TextView) rowView;
            //set text
            textViewName.setText(values[position]);

            return textViewName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            TextView textViewName = (TextView) rowView;
            //set text
            textViewName.setText(values[position]);

            return textViewName;
        }
    }

    /**
     * On creating fragment refresh main list, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;
        //initialize arrays
        weekDays = getResources().getStringArray(R.array.days_week);
        monthDays = getResources().getStringArray(R.array.days_month);

        final View view = inflater.inflate(R.layout.fragment_regular_income, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listViewRegularIncomes);
        listView.setOnItemClickListener(new RegularIncomeItemClickListener());
        refreshRegularIncomeList(listView);

        //on clicking add button show edit box
        View.OnClickListener btnAddRegularIncomeClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.regularIncomeOperationFlag = MainActivity.BudgetWiseAppState.REGULARINCOME_ADD;
                showRegularIncomeEditBox(null, 0);
            }
        };
        Button btnAddRegularIncome = (Button) view.findViewById(R.id.buttonAddRegularIncome);
        btnAddRegularIncome.setOnClickListener(btnAddRegularIncomeClick);

        //on clicking Cancel button in edit box hide edit box
        View.OnClickListener btnEditRegularIncomeCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideRegularIncomeEditBox(v);
            }
        };
        Button btnEditRegularIncomeCancel = (Button) view.findViewById(R.id.buttonRegularIncomeEditCancel);
        btnEditRegularIncomeCancel.setOnClickListener(btnEditRegularIncomeCancelClick);

        //on clicking OK button in edit box perform action, hide edit box
        View.OnClickListener btnEditRegularIncomeOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText regularIncomeNameEdit = (EditText) activity.findViewById(R.id.editTextRegularIncomeName);
                EditText regularIncomeAmountEdit = (EditText) activity.findViewById(R.id.editTextRegularIncomeAmount);
                Spinner regularIncomeDayNumberEdit = (Spinner) activity.findViewById(R.id.spinnerRegularIncomeDayNum);
                RadioButton radioButtonMonthly = (RadioButton) activity.findViewById(R.id.radioButtonRegularIncomeMonthly);
                RadioButton radioButtonWeekly = (RadioButton) activity.findViewById(R.id.radioButtonRegularIncomeWeekly);
                ListView listView = (ListView) view.findViewById(R.id.listViewRegularIncomes);

                //get parameters
                BigDecimal amount;
                if (regularIncomeAmountEdit.getText().length() > 0) {
                    amount = new BigDecimal(regularIncomeAmountEdit.getText().toString());
                } else amount = new BigDecimal("0");
                int dayNum = regularIncomeDayNumberEdit.getSelectedItemPosition() + 1;
                String startDate = activity.getCurrentDateStr();
                String name = regularIncomeNameEdit.getText().toString().trim();
                boolean isMonthly = false;
                if (radioButtonMonthly.isChecked()) {
                    isMonthly = true;
                } else if (radioButtonWeekly.isChecked()) {
                    isMonthly = false;
                }
                //perform action and hide box
                //if name is empty or date is empty or amount=0 do nothing
                if (((name.length() != 0) && (startDate.length() != 0) && (amount.compareTo(new BigDecimal("0")) == 1)) || (activity.budgetWiseAppState.regularIncomeOperationFlag == MainActivity.BudgetWiseAppState.REGULARINCOME_DELETE)) {
                    RegularIncomesChanger changer = new RegularIncomesChanger();
                    switch (activity.budgetWiseAppState.regularIncomeOperationFlag) {
                        case MainActivity.BudgetWiseAppState.REGULARINCOME_ADD:
                            changer.execute(RegularIncomesChanger.ADD, null, name, isMonthly, dayNum, startDate, amount, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.REGULARINCOME_EDIT:
                            changer.execute(RegularIncomesChanger.UPDATE, activity.budgetWiseAppState.currentRegularIncomeId, name, isMonthly, dayNum, startDate, amount, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.REGULARINCOME_DELETE:
                            changer.execute(RegularIncomesChanger.DELETE, activity.budgetWiseAppState.currentRegularIncomeId, null, null, null, null, null, listView);
                            break;
                    }
                    hideRegularIncomeEditBox(v);
                }
            }
        };
        Button btnEditRegularIncomeOK = (Button) view.findViewById(R.id.buttonRegularIncomeEditOK);
        btnEditRegularIncomeOK.setOnClickListener(btnEditRegularIncomeOkClick);

        //on selecting weekly/monthly parameter change the list of possible income days
        RadioGroup.OnCheckedChangeListener radioGroupMonthlyChange = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.radioButtonRegularIncomeMonthly){
                    setDayListMode(true, 1);
                } else if (checkedId == R.id.radioButtonRegularIncomeWeekly){
                    setDayListMode(false, 1);
                }
            }
        };
        RadioGroup radioGroupMonthly = (RadioGroup) view.findViewById(R.id.radioGroupMonthly);
        radioGroupMonthly.setOnCheckedChangeListener(radioGroupMonthlyChange);

        return  view;
    }

    /**
     * Refresh regular income list
     * @param listView List view
     */
    protected void refreshRegularIncomeList(ListView listView){
        RegularIncomesListRefresher refresher = new RegularIncomesListRefresher();
        refresher.execute(listView);
    }

    /**
     * Change the list of possible income days
     * @param isMonthly Weekly/Monthly mode
     * @param dayNum Previous day num
     */
    private void setDayListMode(boolean isMonthly, int dayNum){
        Spinner regularIncomeDayNumberEdit = (Spinner) activity.findViewById(R.id.spinnerRegularIncomeDayNum);
        DaysArrayAdapter daysListAdapter;
        if (dayNum < 1) dayNum = 1;
        if (isMonthly) {
            if (dayNum > monthDays.length) dayNum = monthDays.length;
            daysListAdapter = new DaysArrayAdapter(activity, monthDays);
        } else {
            if (dayNum > weekDays.length) dayNum = weekDays.length;
            daysListAdapter = new DaysArrayAdapter(activity, weekDays);
        }
        regularIncomeDayNumberEdit.setAdapter(daysListAdapter);
        regularIncomeDayNumberEdit.setSelection(dayNum - 1);
    }

    /**
     * Show edit box, hide list, set operation flag, fill fields, etc
     * @param adapter List view adapter
     * @param position Values list position
     */
    protected void showRegularIncomeEditBox(RegularIncomesArrayAdapter adapter, int position) {
        TextView regularIncomeEditText = (TextView) activity.findViewById(R.id.textViewRegularIncomeEdit);
        EditText regularIncomeNameEdit = (EditText) activity.findViewById(R.id.editTextRegularIncomeName);
        EditText regularIncomeAmountEdit = (EditText) activity.findViewById(R.id.editTextRegularIncomeAmount);
        EditText regularIncomeStartDateEdit = (EditText) activity.findViewById(R.id.editTextRegularIncomeStartDate);
        RadioButton radioButtonMonthly = (RadioButton) activity.findViewById(R.id.radioButtonRegularIncomeMonthly);
        RadioButton radioButtonWeekly = (RadioButton) activity.findViewById(R.id.radioButtonRegularIncomeWeekly);
        LinearLayout regularIncomeEditLayout = (LinearLayout) activity.findViewById(R.id.layoutRegularIncomeEdit);
        LinearLayout regularIncomeEditLayoutMain = (LinearLayout) activity.findViewById(R.id.layoutRegularIncomeEditMain);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutRegularIncomesList);

        layoutList.setVisibility(View.GONE);
        //adjust edit box to action
        switch (activity.budgetWiseAppState.regularIncomeOperationFlag) {
            case MainActivity.BudgetWiseAppState.REGULARINCOME_ADD:
                activity.budgetWiseAppState.currentRegularIncomeId = 0;
                activity.setCurrentDate();
                regularIncomeEditText.setText(R.string.regular_income_add);
                regularIncomeNameEdit.setText("");
                regularIncomeAmountEdit.setText(R.string.amount_zero);
                regularIncomeStartDateEdit.setText(activity.getCurrentLocaleDateStr());
                radioButtonMonthly.setChecked(false);
                radioButtonWeekly.setChecked(true);
                regularIncomeEditLayoutMain.setVisibility(View.VISIBLE);
                regularIncomeNameEdit.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(regularIncomeNameEdit, InputMethodManager.SHOW_IMPLICIT);
                break;
            case MainActivity.BudgetWiseAppState.REGULARINCOME_EDIT:
                if ((adapter != null) && (position >= 0)) {
                    activity.budgetWiseAppState.currentRegularIncomeId = adapter.values.get(position).id;
                    activity.setDate(adapter.values.get(position).startDate);
                    regularIncomeEditText.setText(R.string.regular_income_edit);
                    regularIncomeNameEdit.setText(adapter.values.get(position).name);
                    regularIncomeAmountEdit.setText(adapter.values.get(position).amount.toString());
                    regularIncomeStartDateEdit.setText(activity.getCurrentLocaleDateStr());
                    if (adapter.values.get(position).isMonthly) {
                        radioButtonMonthly.setChecked(true);
                        radioButtonWeekly.setChecked(false);
                    } else {
                        radioButtonMonthly.setChecked(false);
                        radioButtonWeekly.setChecked(true);
                    }
                    regularIncomeEditLayoutMain.setVisibility(View.VISIBLE);
                }
                break;
            case MainActivity.BudgetWiseAppState.REGULARINCOME_DELETE:
                activity.budgetWiseAppState.currentRegularIncomeId = adapter.values.get(position).id;
                regularIncomeEditText.setText(R.string.regular_income_delete_q);
                regularIncomeEditLayoutMain.setVisibility(View.GONE);
                break;
        }
        regularIncomeEditLayout.setVisibility(View.VISIBLE);
        if (activity.budgetWiseAppState.regularIncomeOperationFlag == MainActivity.BudgetWiseAppState.REGULARINCOME_EDIT) {
            setDayListMode(adapter.values.get(position).isMonthly, adapter.values.get(position).dayNum);
        } else if (activity.budgetWiseAppState.regularIncomeOperationFlag == MainActivity.BudgetWiseAppState.REGULARINCOME_ADD) {
            setDayListMode(false, 1);
        }
    }

    /**
     * Hide edit box, show list, reset operation flag, hide keyboard
     * @param v Edit box view
     */
    protected void hideRegularIncomeEditBox(View v) {
        activity.budgetWiseAppState.regularIncomeOperationFlag = MainActivity.BudgetWiseAppState.REGULARINCOME_NO_OPERATION;

        LinearLayout layoutEdit = (LinearLayout) activity.findViewById(R.id.layoutRegularIncomeEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutRegularIncomesList);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        layoutEdit.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
    }
}


