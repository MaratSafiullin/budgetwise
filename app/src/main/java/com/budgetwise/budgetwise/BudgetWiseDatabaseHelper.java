package com.budgetwise.budgetwise;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Database handler. CRUD.
 */
public class BudgetWiseDatabaseHelper extends SQLiteOpenHelper {
    //
    private Context context;
    // database name
    private static final String DATABASE_NAME = "BudgetWise.db";
    // database version
    private static final int DATABASE_VERSION = 1;

    /**
     * Constructor
     * @param context
     */
    public BudgetWiseDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * Database creation when it doesn't exist.
     * Read query strings from resources, execute them for creation. Fill DB with default categories
     * @param database
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        createDatabase(database);

        ContentValues values = new ContentValues();
        values.put("name", "Food");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Clothes");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Misc");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Car");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Flat");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Movies");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Pets");
        database.insert("tbl_Categories", null, values);
        values.put("name", "Medicine");
        database.insert("tbl_Categories", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("date", "2016-05-12");
        values.put("amount", 12);
        values.put("comment", "");
        database.insert("tbl_Expenses", null, values);
        values.put("amount", 120);
        database.insert("tbl_Expenses", null, values);
        values.put("date", "2016-05-15");
        values.put("amount", 56);
        database.insert("tbl_Expenses", null, values);
        values.put("date", "2016-05-04");
        values.put("amount", 79);
        database.insert("tbl_Expenses", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("expense_id", 1);
        values.put("category_id", 2);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("category_id", 1);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("expense_id", 2);
        values.put("category_id", 5);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("category_id", 1);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("category_id", 4);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("expense_id", 3);
        values.put("category_id", 6);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("expense_id", 4);
        values.put("category_id", 3);
        database.insert("tbl_Expenses_to_Categories", null, values);
        values.put("category_id", 6);
        database.insert("tbl_Expenses_to_Categories", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("start_date", "2016-04-12");
        values.put("last_income_date", "");
        values.put("amount", 545);
        values.put("name", "My Job");
        values.put("is_monthly", 0);
        values.put("day_number", 2);
        database.insert("tbl_RegularIncomes", null, values);
        values.put("start_date", "2015-02-10");
        values.put("amount", 1965);
        values.put("name", "Prev Job");
        values.put("is_monthly", 1);
        values.put("day_number", 12);
        database.insert("tbl_RegularIncomes", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("date", "2016-05-02");
        values.put("amount", 545);
        values.put("regular_income", "1");
        values.put("comment", "My Job");
        database.insert("tbl_Incomes", null, values);
        values.put("date", "2016-05-12");
        values.put("amount", 873);
        values.putNull("regular_income");
        values.put("comment", "");
        database.insert("tbl_Incomes", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("name", "Budget 1");
        values.put("amount", 140);
        values.put("is_monthly", 0);
        database.insert("tbl_Budgets", null, values);
        values.put("name", "Budget 2");
        values.put("amount", 100);
        values.put("is_monthly", 0);
        database.insert("tbl_Budgets", null, values);
        values.put("name", "Budget 3");
        values.put("amount", 300);
        values.put("is_monthly", 1);
        database.insert("tbl_Budgets", null, values);
        ///////////////////////////////////////////////
        values.clear();
        values.put("budget_id", 1);
        values.put("category_id", 2);
        database.insert("tbl_Budgets_to_Categories", null, values);
        values.put("category_id", 1);
        database.insert("tbl_Budgets_to_Categories", null, values);
        values.put("budget_id", 2);
        values.put("category_id", 5);
        database.insert("tbl_Budgets_to_Categories", null, values);
        values.put("category_id", 1);
        database.insert("tbl_Budgets_to_Categories", null, values);
        values.put("category_id", 4);
        database.insert("tbl_Budgets_to_Categories", null, values);
        values.put("budget_id", 3);
        values.put("category_id", 6);
        database.insert("tbl_Budgets_to_Categories", null, values);
    }

    /**
     * Change DB when version is changed. (Empty)
     * @param database
     * @param i
     * @param i1
     */
    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {

    }

    /**
     * Create database tables etc
     */
    protected void createDatabase(SQLiteDatabase database){
        String dbCreateScript;
        dbCreateScript = this.context.getString(R.string.db_create_categories);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_expenses);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_goals);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_regular_incomes);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_incomes);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_expenses_to_categories);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_budgets);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_budgets_to_categories);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_view_expenses_categories);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_view_budgets_categories);
        database.execSQL(dbCreateScript);
        dbCreateScript = this.context.getString(R.string.db_create_view_budgets_expenses);
        database.execSQL(dbCreateScript);
    }

    /**
     * Recreate all tables
     */
    protected void clearDatabase(){
        SQLiteDatabase database = this.getWritableDatabase();

        database.execSQL("DROP TABLE IF EXISTS tbl_Categories");
        database.execSQL("DROP TABLE IF EXISTS  tbl_Expenses");
        database.execSQL("DROP TABLE IF EXISTS tbl_Goals");
        database.execSQL("DROP TABLE IF EXISTS tbl_RegularIncomes");
        database.execSQL("DROP TABLE IF EXISTS tbl_Incomes");
        database.execSQL("DROP TABLE IF EXISTS tbl_Budgets");
        database.execSQL("DROP TABLE IF EXISTS tbl_Expenses_to_Categories");
        database.execSQL("DROP TABLE IF EXISTS tbl_Budgets_to_Categories");
        database.execSQL("DROP VIEW IF EXISTS vw_ExpensesCategories");
        database.execSQL("DROP VIEW IF EXISTS vw_BudgetsCategories");
        database.execSQL("DROP VIEW IF EXISTS vw_BudgetsExpenses");

        createDatabase(database);

        database.close();

    }

    //Class to store data about Category Record
    protected static class CategoryRec {
        public int id;
        public String name;

        public CategoryRec(int id, String name){
            this.id = id;
            this.name = name;
        }
    }

    //Class to store data about Category Record
    protected static class CategorySelectionRec extends CategoryRec{
        public boolean selected;

        public CategorySelectionRec(int id, String name, boolean selected){
            super(id, name);
            this.selected = selected;
        }
    }

    //Class to store data about Income Record
    protected static class IncomeRec {
        public int id;
        public String date;
        public BigDecimal amount;
        public String comment;
        public int regularIncomeId;

        public IncomeRec(int id, String date, BigDecimal amount, String comment, int regularIncomeId) {
            this.id = id;
            this.date = date;
            this.amount = amount;
            this.comment = comment;
            this.regularIncomeId = regularIncomeId;
        }
    }

    //Class to store data about Regular Income Record
    protected static class RegularIncomeRec {
        public int id;
        public String name;
        public boolean isMonthly;
        public int dayNum;
        public String startDate;
        public String lastIncomeDate;
        public BigDecimal amount;

        public RegularIncomeRec(int id, String name, boolean isMonthly, int dayNum, String startDate, String lastIncomeDate, BigDecimal amount) {
            this.id = id;
            this.name = name;
            this.isMonthly = isMonthly;
            this.dayNum = dayNum;
            this.startDate = startDate;
            this.lastIncomeDate = lastIncomeDate;
            this.amount = amount;
        }
    }

    //Class to store data about Spending Record
    protected static class SpendingRec {
        public int id;
        public String date;
        public BigDecimal amount;
        public String comment;
        public ArrayList<CategoryRec> categories = new ArrayList<>();

        public SpendingRec(int id, String date, BigDecimal amount, String comment) {
            this.id = id;
            this.date = date;
            this.amount = amount;
            this.comment = comment;
        }
    }

    //Class to store data about Budget Record
    protected static class BudgetRec {
        public int id;
        public String name;
        public boolean isMonthly;
        public BigDecimal amount;
        public ArrayList<CategoryRec> categories = new ArrayList<>();

        public BudgetRec(int id, String name, boolean isMonthly, BigDecimal amount) {
            this.amount = amount;
            this.isMonthly = isMonthly;
            this.name = name;
            this.id = id;
        }
    }

    //Class to store data about Day Total
    protected static class DayRec {
        public String date;
        public BigDecimal amount;

        public DayRec(String date, BigDecimal amount) {
            this.date = date;
            this.amount = amount;
        }
    }

    //Class to store data about Category Total
        protected static class CategoryTotalRec {
        public int id;
        public String name;
        public BigDecimal amount;
        public ArrayList<SpendingRec> spending = new ArrayList<>();

        public CategoryTotalRec(int id, String name, BigDecimal amount) {
            this.id = id;
            this.name = name;
            this.amount = amount;
        }
    }

    //Class to store data about Budget Total
    protected static class BudgetTotalRec {
        public int id;
        public String name;
        public BigDecimal setAmount;
        public BigDecimal actualAmount;
        public ArrayList<SpendingRec> spending = new ArrayList<>();

        public BudgetTotalRec(int id, String name, BigDecimal setAmount, BigDecimal actualAmount) {
            this.id = id;
            this.name = name;
            this.setAmount = setAmount;
            this.actualAmount = actualAmount;
        }
    }

    //Class to store data about Expense-to-Category Record
    protected static class SpendingToCategoryRec {
        public int spendingId;
        public int categoryId;

        public SpendingToCategoryRec(int spendingId, int categoryId) {
            this.spendingId = spendingId;
            this.categoryId = categoryId;
        }
    }

    //Class to store data about Budget-to-Category Record
    protected static class BudgetToCategoryRec {
        public int budgetId;
        public int categoryId;

        public BudgetToCategoryRec(int budgetId, int categoryId) {
            this.budgetId = budgetId;
            this.categoryId = categoryId;
        }
    }

    //***********************************************************************************************************************

    /**
     * Get a list of all spending categories sorted by name
     * @return List of categories
     * @throws Exception Any exception
     */
    protected ArrayList<CategoryRec> getCategoriesList() throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<CategoryRec> result = new ArrayList<>();
        Cursor cursor = database.query("tbl_Categories", new String[]{"_id", "name"}, null, null, null, null, "name");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            result.add(new CategoryRec(id, name));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new category record
     * @param name Category name
     * @return New record id
     * @throws Exception Any exception
     */
    protected int addCategory(String name) throws Exception {
        name = name.trim();
        if (name != "") {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", name);
            database.insert("tbl_Categories", null, values);
            Cursor cursor = database.rawQuery("SELECT last_insert_rowid()", null);
            cursor.moveToFirst();
            int lastId = cursor.getInt(0);
            cursor.close();
            database.close();
            return lastId;
        }
        return 0;
    }

    /**
     * Create new category record
     * @param name Category name
     * @return New record id
     * @throws Exception Any exception
     */
    protected boolean addCategoryFromBackup(int id, String name) throws Exception {
        name = name.trim();
        if (name != "") {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("_id", id);
            values.put("name", name);
            database.insert("tbl_Categories", null, values);
            database.close();
        }
        return true;
    }

    /**
     * Update category record
     * @param id Updated record id
     * @param name Category name
     * @return True
     * @throws Exception
     */
    protected boolean updateCategory(int id, String name) throws Exception {
        name = name.trim();
        if (name != "") {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", name);
            database.update("tbl_Categories", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Delete category record
     * @param id Deleted record id
     * @param replacementId Id of the category (record) to replace the deleted one in spending and budget records
     * @return True
     * @throws Exception
     */
    protected boolean deleteCategory(int id, int replacementId) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        if (replacementId != 0) {
            ContentValues values = new ContentValues();
            values.put("category_id", replacementId);
            database.update("tbl_Expenses_to_Categories", values, "category_id = ?", new String[]{String.valueOf(id)});
            database.update("tbl_Budgets_to_Categories", values, "category_id = ?", new String[]{String.valueOf(id)});
        }

        database.delete("tbl_Expenses_to_Categories", "category_id = ?", new String[]{String.valueOf(id)});
        database.delete("tbl_Budgets_to_Categories", "category_id = ?", new String[]{String.valueOf(id)});
        database.delete("tbl_Categories", "_id = ?", new String[]{String.valueOf(id)});
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of income records sorted by date for a given time period
     * @param startDate Period start
     * @param endDate Period end
     * @return List of income records
     * @throws Exception Any exception
     */
    protected ArrayList<IncomeRec> getIncomeList(String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<IncomeRec> result = new ArrayList<>();
        Cursor cursor;
        if ((startDate.length() != 0) && (endDate.length() != 0)) {
            cursor = database.query("tbl_Incomes", new String[]{"_id", "date", "amount", "comment", "regular_income"}, "date >= ? AND date <= ?", new String[] {startDate, endDate}, null, null, "date");
        } else {
            cursor = database.query("tbl_Incomes", new String[]{"_id", "date", "amount", "comment", "regular_income"}, null, null, null, null, "date");
        }
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            String comment = cursor.getString(cursor.getColumnIndex("comment"));
            int regularIncomeId = cursor.getInt(cursor.getColumnIndex("regular_income"));
            result.add(new IncomeRec(id, date, amount, comment, regularIncomeId));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new income record
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @param regularIncomeId Regular income record (if any) related to a new income record. Filled on creating record automatically
     * @return New record id
     * @throws Exception Any exception
     */
    protected int addIncome(String date, BigDecimal amount, String comment, int regularIncomeId) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1)  && (date.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            if (regularIncomeId != 0) {
                values.put("regular_income", regularIncomeId);
            } else {
                values.putNull("regular_income");
            }
            database.insert("tbl_Incomes", null, values);
            Cursor cursor = database.rawQuery("SELECT last_insert_rowid()", null);
            cursor.moveToFirst();
            int lastId = cursor.getInt(0);
            cursor.close();
            database.close();
            return lastId;
        }
        return 0;
    }

    /**
     * Create new income record
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @param regularIncomeId Regular income record (if any) related to a new income record. Filled on creating record automatically
     * @return New record id
     * @throws Exception Any exception
     */
    protected boolean addIncomeFromBackup(int id, String date, BigDecimal amount, String comment, int regularIncomeId) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1)  && (date.length() != 0)){
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("_id", id);
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            values.put("regular_income", regularIncomeId);
            database.insert("tbl_Incomes", null, values);
            database.close();
        }
        return true;
    }

    /**
     * Update income record
     * @param id Updated record id
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @return True
     * @throws Exception
     */
    protected boolean updateIncome(int id, String date, BigDecimal amount, String comment) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1)  && (date.length() != 0)){
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            database.update("tbl_Incomes", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Delete income record
     * @param id Deleted record id
     * @return True
     * @throws Exception
     */
    protected boolean deleteIncome(int id) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_Incomes", "_id = ?", new String[]{String.valueOf(id)});
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of all regular income records sorted by id (creation order)
     * @return List of all regular income records
     * @throws Exception Any exception
     */
    protected ArrayList<RegularIncomeRec> getRegularIncomesList() throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<RegularIncomeRec> result = new ArrayList<>();
        Cursor cursor = database.query("tbl_RegularIncomes", new String[]{"_id", "name", "is_monthly", "day_number", "start_date", "last_income_date", "amount"}, null, null, null, null, "_id");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            boolean isMonthly = (cursor.getInt(cursor.getColumnIndex("is_monthly")) == 1);
            int dayNum = cursor.getInt(cursor.getColumnIndex("day_number"));
            String startDate = cursor.getString(cursor.getColumnIndex("start_date"));
            String lastIncomeDate = cursor.getString(cursor.getColumnIndex("last_income_date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            result.add(new RegularIncomeRec(id, name, isMonthly, dayNum, startDate, lastIncomeDate, amount));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new regular income record
     * @param name Source (job) name
     * @param isMonthly True if it's monthly, false if weekly
     * @param dayNum Day of month (1 to 28) or week (1 to 7) of salary
     * @param startDate From which date the source starts
     * @param amount Amount
     * @return New record id
     * @throws Exception Any exception
     */
    protected int addRegularIncome(String name, boolean isMonthly, int dayNum, String startDate, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((name != "") && (amount.compareTo(new BigDecimal("0")) == 1)  && (startDate.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            if (dayNum < 1) dayNum = 1;
            if (!isMonthly) {
                if (dayNum > 7) dayNum = 7;
            } else {
                if (dayNum > 28) dayNum = 28;
            }
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("day_number", dayNum);
            values.put("start_date", startDate);
            values.put("last_income_date", "");
            values.put("amount", amount.toString());
            database.insert("tbl_RegularIncomes", null, values);
            Cursor cursor = database.rawQuery("SELECT last_insert_rowid()", null);
            cursor.moveToFirst();
            int lastId = cursor.getInt(0);
            cursor.close();
            database.close();
            return lastId;
        }
        return 0;
    }

    /**
     * Create new regular income record
     * @param name Source (job) name
     * @param isMonthly True if it's monthly, false if weekly
     * @param dayNum Day of month (1 to 28) or week (1 to 7) of salary
     * @param startDate From which date the source starts
     * @param amount Amount
     * @return New record id
     * @throws Exception Any exception
     */
    protected boolean addRegularIncomeFromBackup(int id, String name, boolean isMonthly, int dayNum, String startDate, String lastIncomeDate, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((name != "") && (amount.compareTo(new BigDecimal("0")) == 1) && (startDate.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            if (dayNum < 1) dayNum = 1;
            if (!isMonthly) {
                if (dayNum > 7) dayNum = 7;
            } else {
                if (dayNum > 28) dayNum = 28;
            }
            ContentValues values = new ContentValues();
            values.put("_id", id);
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("day_number", dayNum);
            values.put("start_date", startDate);
            values.put("last_income_date", lastIncomeDate);
            values.put("amount", amount.toString());
            database.insert("tbl_RegularIncomes", null, values);
            database.close();
        }
        return true;
    }

    /**
     * Update regular income record
     * @param id Updated record id
     * @param name Source (job) name
     * @param isMonthly True if it's monthly, false if weekly
     * @param dayNum Day of month (1 to 28) or week (1 to 7) of salary
     * @param startDate From which date the source starts
     * @param amount Amount
     * @return True
     * @throws Exception
     */
    protected boolean updateRegularIncome(int id, String name, boolean isMonthly, int dayNum, String startDate, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((name != "") && (amount.compareTo(new BigDecimal("0")) == 1) && (startDate.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            if (dayNum < 1) dayNum = 1;
            if (!isMonthly) {
                if (dayNum > 7) dayNum = 7;
            } else {
                if (dayNum > 28) dayNum = 28;
            }
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("day_number", dayNum);
            values.put("start_date", startDate);
            values.put("amount", amount.toString());
            database.update("tbl_RegularIncomes", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Update regular income record. Set new day of last income. Used when creating income records automatically
     * @param id Updated record id
     * @param lastIncomeDate Date of last income record was automatically created
     * @return True
     * @throws Exception
     */
    protected boolean updateRegularIncomeLastDate(int id, String lastIncomeDate) throws Exception {
        if ((lastIncomeDate.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("last_income_date", lastIncomeDate);
            database.update("tbl_RegularIncomes", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Delete regular income record
     * @param id Deleted record id
     * @return True
     * @throws Exception
     */
    protected boolean deleteRegularIncome(int id) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_RegularIncomes", "_id = ?", new String[]{String.valueOf(id)});
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of spending records sorted by date for a given time period
     * @param startDate Period start
     * @param endDate Period end
     * @return List of spending records
     * @throws Exception Any exception
     */
    protected ArrayList<SpendingRec> getSpendingList(String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<SpendingRec> result = new ArrayList<>();
        Cursor cursor;
        if ((startDate.length() != 0) && (endDate.length() != 0)) {
            cursor = database.query("tbl_Expenses", new String[]{"_id", "date", "amount", "comment"}, "date >= ? AND date <= ?", new String[]{startDate, endDate}, null, null, "date");
        } else {
            cursor = database.query("tbl_Expenses", new String[]{"_id", "date", "amount", "comment"}, null, null, null, null, "date");
        }
        SpendingRec newRec;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            String comment = cursor.getString(cursor.getColumnIndex("comment"));
            newRec = new SpendingRec(id, date, amount, comment);
            newRec.categories = getSpendingCategories(id);
            result.add(newRec);
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new spending record
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @return New record id
     * @throws Exception Any exception
     */
    protected int addSpending(String date, BigDecimal amount, String comment) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (date.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            database.insert("tbl_Expenses", null, values);
            Cursor cursor = database.rawQuery("SELECT last_insert_rowid()", null);
            cursor.moveToFirst();
            int lastId = cursor.getInt(0);
            cursor.close();
            database.close();
            return lastId;
        }
        return 0;
    }

    /**
     * Create new spending record
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @return New record id
     * @throws Exception Any exception
     */
    protected boolean addSpendingFromBackup(int id, String date, BigDecimal amount, String comment) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (date.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("_id", id);
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            database.insert("tbl_Expenses", null, values);
            database.close();
        }
        return true;
    }

    /**
     * Update spending record
     * @param id Updated record id
     * @param date Date
     * @param amount Amount
     * @param comment Comment
     * @return True
     * @throws Exception
     */
    protected boolean updateSpending(int id, String date, BigDecimal amount, String comment) throws Exception {
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (date.length() != 0)) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("date", date);
            values.put("amount", amount.toString());
            values.put("comment", comment);
            database.update("tbl_Expenses", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Delete spending record
     * @param id Deleted record id
     * @return True
     * @throws Exception
     */
    protected boolean deleteSpending(int id) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_Expenses_to_Categories", "expense_id = ?", new String[]{String.valueOf(id)});
        database.delete("tbl_Expenses", "_id = ?", new String[]{String.valueOf(id)});
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of spending categories related to a spending record, sorted by name
     * @param spendingId Id of the spending record
     * @return List of categories
     * @throws Exception Any exception
     */
    protected ArrayList<CategoryRec> getSpendingCategories(int spendingId) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<CategoryRec> result = new ArrayList<>();
        Cursor cursor = database.query("vw_ExpensesCategories", new String[]{"cat_id", "cat_name"}, "exp_id = ?", new String[]{String.valueOf(spendingId)}, null, null, "cat_name");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("cat_id"));
            String name = cursor.getString(cursor.getColumnIndex("cat_name"));
            result.add(new CategoryRec(id, name));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get a list of all spending categories with marks for those related to a spending record, sorted by name
     * @param spendingId Id of the spending record
     * @return List of categories
     * @throws Exception Any exception
     */
    protected ArrayList<CategorySelectionRec> getSpendingSelectedCategories(int spendingId) throws Exception{
        ArrayList<CategorySelectionRec> result = new ArrayList<>();
        ArrayList<CategoryRec> allCategories = getCategoriesList();
        ArrayList<CategoryRec> spendingCategories = null;
        if (spendingId != 0) spendingCategories = getSpendingCategories(spendingId);
        //for each record check if there is a matching record in the given spending record categories list, set "selected" var
        for (int i = 0; i < allCategories.size(); i++) {
            int id = allCategories.get(i).id;
            String name = allCategories.get(i).name;
            boolean selected = false;
            if (spendingId != 0) {
                for (int j = 0; j < spendingCategories.size(); j++) {
                    if (id == spendingCategories.get(j).id) {
                        selected = true;
                        break;
                    }
                }
            }
            result.add(new CategorySelectionRec(id, name, selected));
        }
        return result;
    }

    /**
     * Set a new list of categories selected for a spending record.
     * Delete old records, create new in the DB
     * @param spendingId Spending record id
     * @param categoriesList The list of categories with selection marks
     * @return True
     * @throws Exception Any exception
     */
    protected boolean setSpendingCategories(int spendingId, ArrayList<CategorySelectionRec> categoriesList) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_Expenses_to_Categories", "expense_id = ?", new String[]{String.valueOf(spendingId)});
        ContentValues values = new ContentValues();
        values.put("expense_id", spendingId);
        for (int i = 0; i < categoriesList.size(); i++) {
            if (categoriesList.get(i).selected){
                values.put("category_id", categoriesList.get(i).id);
                database.insert("tbl_Expenses_to_Categories", null, values);
            }
        }
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of budget records sorted by name (can be filtered: monthly or weekly or all of them)
     * @param rangeMode Filter condition
     * @return List of budget records
     * @throws Exception Any exception
     */
    protected ArrayList<BudgetRec> getBudgetsList(int rangeMode) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<BudgetRec> result = new ArrayList<>();
        Cursor cursor;
        switch (rangeMode){
            case MainActivity.BudgetWiseAppState.RANGE_MONTH:
                cursor = database.query("tbl_Budgets", new String[]{"_id", "name", "is_monthly", "amount"}, "is_monthly = 1", null, null, null, "name");
                break;
            case MainActivity.BudgetWiseAppState.RANGE_WEEK:
                cursor = database.query("tbl_Budgets", new String[]{"_id", "name", "is_monthly", "amount"}, "is_monthly = 0", null, null, null, "name");
                break;
            default:
                cursor = database.query("tbl_Budgets", new String[]{"_id", "name", "is_monthly", "amount"}, null, null, null, null, "name");
                break;
        }
        BudgetRec newRec;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            boolean isMonthly = (cursor.getInt(cursor.getColumnIndex("is_monthly")) == 1);
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            newRec = new BudgetRec(id, name, isMonthly, amount);
            newRec.categories = getSpendingCategories(id);
            result.add(newRec);
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new budget income record
     * @param name Budget name
     * @param isMonthly True if it's monthly, false if weekly
     * @param amount Amount
     * @return New record id
     * @throws Exception Any exception
     */
    protected int addBudget(String name, boolean isMonthly, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (name != "")) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("amount", amount.toString());
            database.insert("tbl_Budgets", null, values);
            Cursor cursor = database.rawQuery("SELECT last_insert_rowid()", null);
            cursor.moveToFirst();
            int lastId = cursor.getInt(0);
            cursor.close();
            database.close();
            return lastId;
        }
        return 0;
    }

    /**
     * Create new budget income record
     * @param name Budget name
     * @param isMonthly True if it's monthly, false if weekly
     * @param amount Amount
     * @return New record id
     * @throws Exception Any exception
     */
    protected boolean addBudgetFromBackup(int id, String name, boolean isMonthly, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (name != "")) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("_id", id);
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("amount", amount.toString());
            database.insert("tbl_Budgets", null, values);
            database.close();
        }
        return true;
    }

    /**
     * Update regular income record
     * @param id Updated record id
     * @param name Source (job) name
     * @param isMonthly True if it's monthly, false if weekly
     * @param amount Amount
     * @return True
     * @throws Exception
     */
    protected boolean updateBudget(int id, String name, boolean isMonthly, BigDecimal amount) throws Exception {
        name = name.trim();
        if ((amount.compareTo(new BigDecimal("0")) == 1) && (name != "")) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("is_monthly", isMonthly);
            values.put("amount", amount.toString());
            database.update("tbl_Budgets", values, "_id = ?", new String[]{String.valueOf(id)});
            database.close();
        }
        return true;
    }

    /**
     * Delete budget record
     * @param id Deleted record id
     * @return True
     * @throws Exception
     */
    protected boolean deleteBudget(int id) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_Budgets_to_Categories", "budget_id = ?", new String[]{String.valueOf(id)});
        database.delete("tbl_Budgets", "_id = ?", new String[]{String.valueOf(id)});
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of spending categories related to a budget record, sorted by name
     * @param budgetId Id of the budget record
     * @return List of categories
     * @throws Exception Any exception
     */
    protected ArrayList<CategoryRec> getBudgetCategories(int budgetId) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<CategoryRec> result = new ArrayList<>();
        Cursor cursor = database.query("vw_BudgetsCategories", new String[]{"cat_id", "cat_name"}, "bdg_id = ?", new String[]{String.valueOf(budgetId)}, null, null, "cat_name");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("cat_id"));
            String name = cursor.getString(cursor.getColumnIndex("cat_name"));
            result.add(new CategoryRec(id, name));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get a list of all spending categories with marks for those related to a budget record, sorted by name
     * @param budgetId Id of the budget record
     * @return List of categories
     * @throws Exception Any exception
     */
    protected ArrayList<CategorySelectionRec> getBudgetSelectedCategories(int budgetId) throws Exception {
        ArrayList<CategorySelectionRec> result = new ArrayList<>();
        ArrayList<CategoryRec> allCategories = getCategoriesList();
        ArrayList<CategoryRec> budgetCategories = null;
        if (budgetId != 0) budgetCategories = getBudgetCategories(budgetId);
        //for each record check if there is a matching record in the given budget record categories list, set "selected" var
        for (int i = 0; i < allCategories.size(); i++) {
            int id = allCategories.get(i).id;
            String name = allCategories.get(i).name;
            boolean selected = false;
            if (budgetId != 0) {
                for (int j = 0; j < budgetCategories.size(); j++) {
                    if (id == budgetCategories.get(j).id) {
                        selected = true;
                        break;
                    }
                }
            }
            result.add(new CategorySelectionRec(id, name, selected));
        }
        return result;
    }

    /**
     * Set a new list of categories selected for a budget record.
     * Delete old records, create new in the DB
     * @param budgetId Budget record id
     * @param categoriesList The list of categories with selection marks
     * @return True
     * @throws Exception Any exception
     */
    protected boolean setBudgetCategories(int budgetId, ArrayList<CategorySelectionRec> categoriesList) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("tbl_Budgets_to_Categories", "budget_id = ?", new String[]{String.valueOf(budgetId)});
        ContentValues values = new ContentValues();
        values.put("budget_id", budgetId);
        for (int i = 0; i < categoriesList.size(); i++) {
            if (categoriesList.get(i).selected){
                values.put("category_id", categoriesList.get(i).id);
                database.insert("tbl_Budgets_to_Categories", null, values);
            }
        }
        database.close();
        return true;
    }

    //***********************************************************************************************************************

    /**
     * Get total spending amount for a given period
     * @param startDate Period start
     * @param endDate Period end
     * @return Total amount
     * @throws Exception Any exception
     */
    protected BigDecimal getTotalSpending(String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        BigDecimal result = new BigDecimal("0");
        Cursor cursor = database.rawQuery("SELECT SUM(amount) FROM tbl_Expenses WHERE date >= ? AND date <= ?", new String[]{startDate, endDate});
        cursor.moveToFirst();
        String amount = cursor.getString(0);
        if (amount != null) {
            result = new BigDecimal(amount);
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get total income amount for a given period
     * @param startDate Period start
     * @param endDate Period end
     * @return Total amount
     * @throws Exception Any exception
     */
    protected BigDecimal getTotalIncome(String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        BigDecimal result = new BigDecimal("0");
        Cursor cursor = database.rawQuery("SELECT SUM(amount) " +
                "FROM tbl_Incomes " +
                "WHERE date >= ? AND date <= ?", new String[]{startDate, endDate});
        cursor.moveToFirst();
        String amount = cursor.getString(0);
        if (amount != null) {
            result = new BigDecimal(amount);
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get total spending amount for every day in a given period
     * @param startDate Period start
     * @param endDate Period end
     * @return Days with amounts
     * @throws Exception Any exception
     */
    protected ArrayList<DayRec> getTotalSpendingDays(String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<DayRec> result = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT date, SUM(amount) AS amount " +
                "FROM tbl_Expenses " +
                "WHERE date >= ? AND date <= ? " +
                "GROUP BY date", new String[]{startDate, endDate});
        while (cursor.moveToNext()) {
            String date = cursor.getString(cursor.getColumnIndex("date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            result.add(new DayRec(date, amount));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get total spending amount for every category for a given period with breakdown by spending records
     * @param startDate Period start
     * @param endDate Period end
     * @return Categories with amounts
     * @throws Exception Any exception
     */
    protected ArrayList<CategoryTotalRec> getTotalSpendingCategories(String startDate, String endDate) throws Exception{
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<CategoryTotalRec> result = new ArrayList<>();
        ArrayList<CategoryTotalRec> subResult = new ArrayList<>();
        //get list of categories that have spending for the period
        Cursor cursor = database.rawQuery("SELECT cat_name, cat_id, SUM(exp_amount) AS amount " +
                "FROM vw_ExpensesCategories " +
                "WHERE exp_date >= ? AND exp_date <= ? " +
                "GROUP BY cat_name, cat_id " +
                "ORDER BY cat_name, cat_id", new String[]{startDate, endDate});
        CategoryTotalRec newRec;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("cat_id"));
            String name = cursor.getString(cursor.getColumnIndex("cat_name"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("amount")));
            newRec = new CategoryTotalRec(id, name, amount);
            //get spending breakdown
            newRec.spending = getCategorySpending(id, startDate, endDate);
            //
            subResult.add(newRec);
        }
        cursor.close();
        database.close();
        //get all categories
        ArrayList<CategoryRec> allCategories = getCategoriesList();
        //for every category create a result record either from those with spending or empty
        for (int i = 0; i < allCategories.size(); i++) {
            boolean hasSpendings = false;
            int id = allCategories.get(i).id;
            String name = allCategories.get(i).name;
            for (int j = 0; j < subResult.size(); j++) {
                if (subResult.get(j).id == id){
                    hasSpendings = true;
                    //category with spending
                    result.add(subResult.get(j));
                    break;
                }
            }
            //empty total rec
            if (!hasSpendings) {
                result.add(new CategoryTotalRec(id, name, new BigDecimal("0")));
            }
        }
        return result;
    }

    /**
     * Get spending records breakdown for a category. Used for totals display
     * @param categoryId Category record id
     * @return Spending list
     * @throws Exception Any exception
     */
    protected ArrayList<SpendingRec> getCategorySpending(int categoryId, String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<SpendingRec> result = new ArrayList<>();
        Cursor cursor = database.query("vw_ExpensesCategories", new String[]{"exp_id", "exp_date", "exp_amount"}, "cat_id = ? AND exp_date >= ? AND exp_date <= ?", new String[]{String.valueOf(categoryId), startDate, endDate}, null, null, "exp_date");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("exp_id"));
            String date = cursor.getString(cursor.getColumnIndex("exp_date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("exp_amount")));
            result.add(new SpendingRec(id, date, amount, ""));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get total spending amount for every budget for a given period with breakdown by spending records
     * @param startDate Period start
     * @param endDate Period end
     * @param isMonthly Type of budgets: monthly/weekly
     * @return Budgets with amounts
     * @throws Exception Any exception
     */
    protected ArrayList<BudgetTotalRec> getTotalSpendingBudgets(String startDate, String endDate, boolean isMonthly) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<BudgetTotalRec> subResult = new ArrayList<>();
        ArrayList<BudgetTotalRec> result = new ArrayList<>();
        //get list of budgets that have spending for the period
        Cursor cursor = database.rawQuery("SELECT bdg_name, bdg_amount, bdg_id, SUM(exp_amount) AS actual_amount " +
                "FROM vw_BudgetsExpenses " +
                "WHERE exp_date >= ? AND exp_date <= ? " +
                "GROUP BY bdg_name, bdg_id " +
                "ORDER BY bdg_name, bdg_id", new String[]{startDate, endDate});
        BudgetTotalRec newRec;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("bdg_id"));
            String name = cursor.getString(cursor.getColumnIndex("bdg_name"));
            BigDecimal setAmount = new BigDecimal(cursor.getString(cursor.getColumnIndex("bdg_amount")));
            BigDecimal actualAmount = new BigDecimal(cursor.getString(cursor.getColumnIndex("actual_amount")));
            newRec = new BudgetTotalRec(id, name, setAmount, actualAmount);
            //get spending breakdown
            newRec.spending = getBudgetSpending(id, startDate, endDate);
            //
            subResult.add(newRec);
        }
        cursor.close();
        database.close();
        //get budgets of a given type
        ArrayList<BudgetRec> allBudgets;
        if (isMonthly) {
            allBudgets = getBudgetsList(MainActivity.BudgetWiseAppState.RANGE_MONTH);
        } else {
            allBudgets = getBudgetsList(MainActivity.BudgetWiseAppState.RANGE_WEEK);
        }
        //for every budget create a result record either from those with spending or empty
        for (int i = 0; i < allBudgets.size(); i++) {
            boolean hasSpending = false;
            int id = allBudgets.get(i).id;
            String name = allBudgets.get(i).name;
            BigDecimal setAmount = allBudgets.get(i).amount;
            for (int j = 0; j < subResult.size(); j++) {
                if (subResult.get(j).id == id){
                    hasSpending = true;
                    //budget with spending
                    result.add(subResult.get(j));
                    break;
                }
            }
            //empty total rec
            if (!hasSpending) {
                result.add(new BudgetTotalRec(id, name, setAmount, new BigDecimal("0")));
            }
        }

        return result;
    }

    /**
     * Get spending records breakdown for a budget. Used for totals display
     * @param budgetId Budget record id
     * @return Spending list
     * @throws Exception Any exception
     */
    protected ArrayList<SpendingRec> getBudgetSpending(int budgetId, String startDate, String endDate) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<SpendingRec> result = new ArrayList<>();
        Cursor cursor = database.query("vw_BudgetsExpenses", new String[]{"exp_id", "exp_date", "exp_amount"}, "bdg_id = ? AND exp_date >= ? AND exp_date <= ?", new String[]{String.valueOf(budgetId), startDate, endDate}, null, null, "exp_date");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("exp_id"));
            String date = cursor.getString(cursor.getColumnIndex("exp_date"));
            BigDecimal amount = new BigDecimal(cursor.getString(cursor.getColumnIndex("exp_amount")));
            result.add(new SpendingRec(id, date, amount, ""));
        }
        cursor.close();
        database.close();
        return result;
    }

    //***********************************************************************************************************************

    /**
     * Check if category is used in spending or budget records
     * @param id Category record id
     * @return True if it is used, false if not
     * @throws Exception Any exception
     */
    protected boolean checkCategoryUsage(int id) throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        boolean result = false;
        Cursor cursorBdg = database.query("vw_BudgetsCategories", new String[]{"cat_id"}, "cat_id = ?", new String[]{String.valueOf(id)}, null, null, null);
        Cursor cursorSpnd = database.query("vw_ExpensesCategories", new String[]{"cat_id"}, "cat_id = ?", new String[]{String.valueOf(id)}, null, null, null);
        if ((cursorBdg.getCount() > 0) || (cursorSpnd.getCount() > 0)){
            result = true;
        }
        cursorSpnd.close();
        cursorBdg.close();
        database.close();
        return result;
    }

    //***********************************************************************************************************************

    /**
     * Get a list of all spending to category matches
     * @return List of matches
     * @throws Exception Any exception
     */
    protected ArrayList<SpendingToCategoryRec> getSpendingToCategories() throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<SpendingToCategoryRec> result = new ArrayList<>();
        Cursor cursor = database.query("tbl_Expenses_to_Categories", new String[]{"expense_id", "category_id"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int spendingId = cursor.getInt(cursor.getColumnIndex("expense_id"));
            int categoryId = cursor.getInt(cursor.getColumnIndex("category_id"));
            result.add(new SpendingToCategoryRec(spendingId, categoryId));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Get a list of all budget to category matches
     * @return List of matches
     * @throws Exception Any exception
     */
    protected ArrayList<BudgetToCategoryRec> getBudgetsToCategories() throws Exception {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<BudgetToCategoryRec> result = new ArrayList<>();
        Cursor cursor = database.query("tbl_Budgets_to_Categories", new String[]{"budget_id", "category_id"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int budgetId = cursor.getInt(cursor.getColumnIndex("budget_id"));
            int categoryId = cursor.getInt(cursor.getColumnIndex("category_id"));
            result.add(new BudgetToCategoryRec(budgetId, categoryId));
        }
        cursor.close();
        database.close();
        return result;
    }

    /**
     * Create new SpendingToCategory record
     * @param spendingId Spending id
     * @param categoryId Category id
     * @throws Exception Any exception
     */
    protected boolean addSpendingToCategoryRecFromBackup(int spendingId, int categoryId) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("expense_id", spendingId);
        values.put("category_id", categoryId);
        database.insert("tbl_Expenses_to_Categories", null, values);
        database.close();
        return true;
    }

    /**
     * Create new BudgetToCategory record
     * @param budgetId Budget id
     * @param categoryId Category id
     * @throws Exception Any exception
     */
    protected boolean addBudgetToCategoryRecFromBackup(int budgetId, int categoryId) throws Exception {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("budget_id", budgetId);
        values.put("category_id", categoryId);
        database.insert("tbl_Budgets_to_Categories", null, values);
        database.close();
        return true;
    }
}
