package com.budgetwise.budgetwise;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Main Activity of the app
 * Uses Navigation Drawer and fragments to switch between modes
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //fragments used to represent application modes, each of them replaces main content of the activity
    protected SpendingFragment spendingFragment;
    protected CategoriesFragment categoriesFragment;
    protected RegularIncomesFragment regularIncomeFragment;
    protected IncomeFragment incomeFragment;
    protected BudgetsFragment budgetsFragment;
    protected TotalsFragment totalsFragment;
    protected BackupRestoreFragment backupRestoreFragment;
    protected StartFragment startFragment;

    //database handler (middle tier)
    protected BudgetWiseDatabaseHelper databaseHelper;
    //object used to keep application state at every moment. like main mode, etc
    protected BudgetWiseAppState budgetWiseAppState = new BudgetWiseAppState();
    //constant for dialog creation
    private final int DIALOG_DATE = 1;
    //link to dialog for choosing dates, used to set it's parameters before showing
    private DatePickerDialog datePickerDialog;
    //unified calendar for all the app (to use similar settings in all functions)
    protected final Calendar calendar = Calendar.getInstance();

    //***********************************************************************************************************************

    /**
     * Class to keep date range (mostly current date range for the app). Used in selection data from DB
     */
    protected class DateRange {
        //range type (month, week)
        protected int currentRangeMode = BudgetWiseAppState.RANGE_MONTH;
        //range limits
        protected Date dateStart;
        protected Date dateEnd;

        /**
         * Constructor
         * @param date Initial date (usually current system date)
         * @param rangeMode Initial mode
         */
        public DateRange(Date date, int rangeMode) {
            setRangeMode(rangeMode);
            setRangeLimits(date);
        }

        /**
         * Set new range limits, depending on the given date
         * @param date Date within the new range
         */
        public void setRangeLimits(Date date) {
            Calendar calendar = MainActivity.this.calendar;
            calendar.setTime(date);
            switch (currentRangeMode) {
                case BudgetWiseAppState.RANGE_MONTH:
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    this.dateStart = calendar.getTime();
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    this.dateEnd = calendar.getTime();
                    break;
                case BudgetWiseAppState.RANGE_WEEK:
                    calendar.set(Calendar.DAY_OF_WEEK, calendar.MONDAY);
                    this.dateStart = calendar.getTime();
                    calendar.set(Calendar.DAY_OF_WEEK, calendar.SUNDAY);
                    this.dateEnd = calendar.getTime();
                    break;
                default:
                    this.dateStart = date;
                    this.dateEnd = date;
                    break;
            }
        }

        /**
         * Sets new range type (month, week). Resets limits using current system date
         * @param rangeMode New range type
         */
        public void setRangeMode(int rangeMode){
            switch (currentRangeMode) {
                case BudgetWiseAppState.RANGE_MONTH:
                    this.currentRangeMode = rangeMode;
                    setRangeLimits(budgetWiseAppState.currentDate);
                    break;
                case BudgetWiseAppState.RANGE_WEEK:
                    this.currentRangeMode = rangeMode;
                    setRangeLimits(budgetWiseAppState.currentDate);
                    break;
                default:
                    this.currentRangeMode = BudgetWiseAppState.RANGE_CUSTOM;
                    setRangeLimits(budgetWiseAppState.currentDate);
                    break;
            }
        }

        /**
         * Get range type
         * @return Range type
         */
        public int getRangeMode(){
            return this.currentRangeMode;
        }

        /**
         * Set next period (week, month)
         */
        public void setNext() {
            Calendar calendar = MainActivity.this.calendar;
            calendar.setTime(this.dateStart);
            switch (currentRangeMode) {
                case BudgetWiseAppState.RANGE_MONTH:
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
                    break;
                case BudgetWiseAppState.RANGE_WEEK:
                    calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 7);
                    break;
            }
            setRangeLimits(calendar.getTime());
        }

        /**
         * Set previos period (week, month)
         */
        public void setPrevious() {
            Calendar calendar = MainActivity.this.calendar;
            calendar.setTime(this.dateStart);
            switch (currentRangeMode) {
                case BudgetWiseAppState.RANGE_MONTH:
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
                    break;
                case BudgetWiseAppState.RANGE_WEEK:
                    calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 7);
                    break;
            }
            setRangeLimits(calendar.getTime());
        }
    }

    /**
     * Class to keep application state at every moment. Like main mode, etc
     */
    protected class BudgetWiseAppState {
        //possible main modes (related to fragments)
        protected static final int MODE_START = 0, MODE_SPENDING = 1, MODE_CATEGORIES = 2, MODE_INCOME = 3, MODE_REGULAR_INCOME = 4, MODE_BUDGETS = 5, MODE_TOTALS = 6, MODE_BACKUP = 7;
        //possible categories mode states (view, add, edit, delete)
        protected static final int CATEGORY_NO_OPERATION = -1, CATEGORY_ADD = 0, CATEGORY_EDIT = 1, CATEGORY_DELETE = 3;
        //possible spending mode states (view, add, edit, delete)
        protected static final int SPENDING_NO_OPERATION = -1, SPENDING_ADD = 0, SPENDING_EDIT = 1, SPENDING_DELETE = 3;
        //possible income mode states (view, add, edit, delete)
        protected static final int INCOME_NO_OPERATION = -1, INCOME_ADD = 0, INCOME_EDIT = 1, INCOME_DELETE = 3;
        //possible regular income mode states (view, add, edit, delete)
        protected static final int REGULARINCOME_NO_OPERATION = -1, REGULARINCOME_ADD = 0, REGULARINCOME_EDIT = 1, REGULARINCOME_DELETE = 3;
        //possible budgets mode states (view, add, edit, delete)
        protected static final int BUDGET_NO_OPERATION = -1, BUDGET_ADD = 0, BUDGET_EDIT = 1, BUDGET_DELETE = 3;
        //possible date picker dialog states (target to set date to)
        protected static final int DATEPICKER_NONE = -1, DATEPICKER_SPENDING = 0, DATEPICKER_INCOME = 1, DATEPICKER_REGULAR_INCOME = 2;
        //possible amount set submode (called from some main modes) states (direct set, or calculating)
        protected static final int AMOUNT_CALCULATE = 0, AMOUNT_SET = 1;
        //possible range modes (week, month)
        protected static final int RANGE_NONE = -1, RANGE_MONTH = 0, RANGE_WEEK = 1, RANGE_CUSTOM = 2;

        //keep main mode setting
        protected int mainMode = MODE_START;
        //keep amount set submode switch (on/off)
        protected boolean editAmountMode = false;
        //keep current category for operations (edit, delete)
        protected int currentCategoryId = 0;
        //keep category to replace one being deleted
        protected int replacementCategoryId = 0;
        //keep category mode state
        protected int categoryOperationFlag = CATEGORY_NO_OPERATION;
        //keep current spending for operations (edit, delete)
        protected int currentSpendingId = 0;
        //keep spending mode state
        protected int spendingOperationFlag = SPENDING_NO_OPERATION;
        //keep current income for operations (edit, delete)
        protected int currentIncomeId = 0;
        //keep income mode state
        protected int incomeOperationFlag = INCOME_NO_OPERATION;
        //keep current regular income for operations (edit, delete)
        protected int currentRegularIncomeId = 0;
        //keep regular income mode state
        protected int regularIncomeOperationFlag = REGULARINCOME_NO_OPERATION;
        //keep current budget for operations (edit, delete)
        protected int currentBudgetId = 0;
        //keep budget mode state
        protected int budgetOperationFlag = BUDGET_NO_OPERATION;

        //current amount being set in amount set submode
        protected int currentAmount;
        //keep amount set submode state
        protected int amountMode = AMOUNT_SET;
        //current target (field) for amount set
        protected int amountTarget = -1;

        //parameters for datepicker dialog
        protected int operationalYear, operationalMonth, operationalDay;
        //current system date kept in the app
        protected Date currentDate;
        //keep date picker dialog state
        protected int currentDatePickerMode = DATEPICKER_NONE;
        //current date range to use in the app (to select data from DB)
        protected DateRange operationalDateRange;
    }

    /**
     * Date Picker Dialog listener
     */
    private final DatePickerDialog.OnDateSetListener datePickerCallBack = new DatePickerDialog.OnDateSetListener() {

        /**
         * Handle date setting:
         * Pass parameters to current application state object
         * Set text representation of the date to the fragment field
         * @param view Datepicker
         * @param year Selected year
         * @param monthOfYear Selected month
         * @param dayOfMonth Selected day
         */
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            //set current state
            budgetWiseAppState.operationalYear = year;
            budgetWiseAppState.operationalMonth = monthOfYear;
            budgetWiseAppState.operationalDay = dayOfMonth;
            //set text to the field
            EditText dateText = null;
            switch (budgetWiseAppState.currentDatePickerMode){
                case BudgetWiseAppState.DATEPICKER_SPENDING:
                    dateText = (EditText) findViewById(R.id.editTextSpendingDate);
                    break;
                case BudgetWiseAppState.DATEPICKER_INCOME:
                    dateText = (EditText) findViewById(R.id.editTextIncomeDate);
                    break;
                case BudgetWiseAppState.DATEPICKER_REGULAR_INCOME:
                    dateText = (EditText) findViewById(R.id.editTextRegularIncomeStartDate);
                    break;
            }
            if (dateText != null) dateText.setText(getCurrentLocaleDateStr());
            //reset mode
            budgetWiseAppState.currentDatePickerMode = BudgetWiseAppState.DATEPICKER_NONE;
        }
    };

    /**
     * Async task to automatically create Income records based on Regular Income sources and current date
     * Run in background when the app starts
     */
    private class IncomesFiller extends AsyncTask {

        /**
         * Check Regular Income source. Go from its start date or the date the last Income record was created to the current date
         * Create Income records at right days of week or month for the considered period
         * @param objects Parameters (not used)
         * @return Null
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            //
            Calendar calendar = MainActivity.this.calendar;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            //get regular sources
            ArrayList<BudgetWiseDatabaseHelper.RegularIncomeRec> regularIncomes = new ArrayList<>();
            try {
                regularIncomes = databaseHelper.getRegularIncomesList();
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "Error! Can't get the list of Regular Income Sources", Toast.LENGTH_LONG).show();
                    }
                });
            }
            //go through each source
            for (int i = 0; i < regularIncomes.size(); i++) {
                //source parameters
                int regularIncomeDay = regularIncomes.get(i).dayNum;
                BigDecimal regularIncomeAmount = regularIncomes.get(i).amount;
                String regularIncomeName = regularIncomes.get(i).name;
                int regularIncomeId = regularIncomes.get(i).id;
                //get dates of the source
                Date startDate = new Date();
                Date lastIncomeDate = new Date();
                try {
                    startDate = format.parse(regularIncomes.get(i).startDate);
                    lastIncomeDate.setTime(startDate.getTime() - 1000*60*60*24);
                    if (regularIncomes.get(i).lastIncomeDate.length() != 0) {
                        lastIncomeDate = format.parse(regularIncomes.get(i).lastIncomeDate);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //get the latest date to start from
                Date date = new Date();
                if (lastIncomeDate.getTime() > startDate.getTime()){
                    date.setTime(lastIncomeDate.getTime() + 1000*60*60*24);
                } else {
                    date.setTime(startDate.getTime());
                }
                calendar.setTime(date);
                //
                Date currentDate = new Date();
                //go through considered period
                while (date.getTime() <= currentDate.getTime()){
                    //add Income records
                    try {
                        if (regularIncomes.get(i).isMonthly){
                            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                            if (dayOfMonth == regularIncomeDay){
                                String dateStr = format.format(date);
                                databaseHelper.addIncome(dateStr, regularIncomeAmount, regularIncomeName, regularIncomeId);
                                databaseHelper.updateRegularIncomeLastDate(regularIncomeId, dateStr);
                            }
                        } else {
                            int dayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1;
                            if (dayOfWeek == regularIncomeDay){
                                String dateStr = format.format(date);
                                databaseHelper.addIncome(dateStr, regularIncomeAmount, regularIncomeName, regularIncomeId);
                                databaseHelper.updateRegularIncomeLastDate(regularIncomeId, dateStr);
                            }
                        }
                    } catch (Exception e) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.this, "Error! Can't add Automatic Income", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    //next date
                    calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
                    date = calendar.getTime();
                }
            }
            return null;
        }
    }

    //***********************************************************************************************************************

    /**
     * On activity creation initialize fragments, calendar, date picker dialog, database handler, etc. Create Income records
     * @param savedInstanceState Activity State to restore
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        //action bar
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //drawer menu
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //database handler
        databaseHelper = new BudgetWiseDatabaseHelper(this);
        //fragments
        spendingFragment = new SpendingFragment();
        categoriesFragment = new CategoriesFragment();
        regularIncomeFragment = new RegularIncomesFragment();
        incomeFragment = new IncomeFragment();
        budgetsFragment = new BudgetsFragment();
        totalsFragment = new TotalsFragment();
        backupRestoreFragment = new BackupRestoreFragment();
        startFragment = new StartFragment();
        //date picker
        setCurrentDate();
        datePickerDialog = new DatePickerDialog(this, datePickerCallBack, budgetWiseAppState.operationalYear, budgetWiseAppState.operationalMonth, budgetWiseAppState.operationalDay);
        //range
        budgetWiseAppState.operationalDateRange = new DateRange(budgetWiseAppState.currentDate, BudgetWiseAppState.RANGE_MONTH);
        //automatic income
        fillAutomaticIncomes();
        //
        if (savedInstanceState == null) {
            if (budgetWiseAppState.mainMode == BudgetWiseAppState.MODE_START) {
                goToStartScreen();
            }
        }
    }

    /**
     * Save the application state
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("mainMode", budgetWiseAppState.mainMode);
        outState.putBoolean("editAmountMode", budgetWiseAppState.editAmountMode);
        outState.putInt("currentCategoryId", budgetWiseAppState.replacementCategoryId);
        outState.putInt("replacementCategoryId", budgetWiseAppState.replacementCategoryId);
        outState.putInt("categoryOperationFlag", budgetWiseAppState.categoryOperationFlag);
        outState.putInt("currentSpendingId", budgetWiseAppState.currentSpendingId);
        outState.putInt("spendingOperationFlag", budgetWiseAppState.spendingOperationFlag);
        outState.putInt("currentIncomeId", budgetWiseAppState.currentIncomeId);
        outState.putInt("incomeOperationFlag", budgetWiseAppState.incomeOperationFlag);
        outState.putInt("currentRegularIncomeId", budgetWiseAppState.currentRegularIncomeId);
        outState.putInt("regularIncomeOperationFlag", budgetWiseAppState.regularIncomeOperationFlag);
        outState.putInt("currentBudgetId", budgetWiseAppState.currentBudgetId);
        outState.putInt("budgetOperationFlag", budgetWiseAppState.budgetOperationFlag);

        outState.putInt("currentAmount", budgetWiseAppState.currentAmount);
        outState.putInt("amountMode", budgetWiseAppState.amountMode);

        outState.putInt("amountTarget", budgetWiseAppState.amountTarget);

        outState.putInt("operationalYear", budgetWiseAppState.operationalYear);
        outState.putLong("currentDate", budgetWiseAppState.currentDate.getTime());
        outState.putInt("currentDatePickerMode", budgetWiseAppState.currentDatePickerMode);
        outState.putInt("operationalDateRange.currentRangeMode", budgetWiseAppState.operationalDateRange.currentRangeMode);
        outState.putLong("operationalDateRange.start", budgetWiseAppState.operationalDateRange.dateStart.getTime());
        outState.putLong("operationalDateRange.end", budgetWiseAppState.operationalDateRange.dateEnd.getTime());

        AmountFragment amountFragment  = (AmountFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentAmount);
        outState.putInt("amountFragment.amount", amountFragment.amount);
    }

    /**
     * Restore the application state, load the last fragment
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        budgetWiseAppState.mainMode = savedInstanceState.getInt("mainMode");
        budgetWiseAppState.editAmountMode = savedInstanceState.getBoolean("editAmountMode");
        budgetWiseAppState.currentCategoryId = savedInstanceState.getInt("currentCategoryId");
        budgetWiseAppState.replacementCategoryId = savedInstanceState.getInt("replacementCategoryId");
        budgetWiseAppState.categoryOperationFlag = savedInstanceState.getInt("categoryOperationFlag");
        budgetWiseAppState.currentSpendingId = savedInstanceState.getInt("currentSpendingId");
        budgetWiseAppState.spendingOperationFlag = savedInstanceState.getInt("spendingOperationFlag");
        budgetWiseAppState.currentIncomeId = savedInstanceState.getInt("currentIncomeId");
        budgetWiseAppState.incomeOperationFlag = savedInstanceState.getInt("incomeOperationFlag");
        budgetWiseAppState.currentRegularIncomeId = savedInstanceState.getInt("currentRegularIncomeId");
        budgetWiseAppState.regularIncomeOperationFlag = savedInstanceState.getInt("regularIncomeOperationFlag");
        budgetWiseAppState.currentBudgetId = savedInstanceState.getInt("currentBudgetId");
        budgetWiseAppState.budgetOperationFlag = savedInstanceState.getInt("budgetOperationFlag");

        budgetWiseAppState.currentAmount = savedInstanceState.getInt("currentAmount");
        budgetWiseAppState.amountMode = savedInstanceState.getInt("amountMode");
        budgetWiseAppState.amountTarget = savedInstanceState.getInt("amountTarget");

        budgetWiseAppState.operationalYear = savedInstanceState.getInt("operationalYear");
        budgetWiseAppState.currentDate.setTime(savedInstanceState.getLong("currentDate"));
        budgetWiseAppState.currentDatePickerMode = savedInstanceState.getInt("currentDatePickerMode");
        budgetWiseAppState.operationalDateRange.currentRangeMode = savedInstanceState.getInt("operationalDateRange.currentRangeMode");
        budgetWiseAppState.operationalDateRange.dateStart.setTime(savedInstanceState.getLong("operationalDateRange.start"));
        budgetWiseAppState.operationalDateRange.dateEnd.setTime(savedInstanceState.getLong("operationalDateRange.end"));

        AmountFragment amountFragment  = (AmountFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentAmount);
        amountFragment.amount = savedInstanceState.getInt("amountFragment.amount");

        //load fragment
        switch (budgetWiseAppState.mainMode){
            case BudgetWiseAppState.MODE_START :
                goToStartScreen();
                break;
            case BudgetWiseAppState.MODE_SPENDING :
                switchFragment(R.id.nav_expenses);
                break;
            case BudgetWiseAppState.MODE_INCOME :
                switchFragment(R.id.nav_income);
                break;
            case BudgetWiseAppState.MODE_TOTALS :
                switchFragment(R.id.nav_totals);
                break;
            case BudgetWiseAppState.MODE_CATEGORIES :
                switchFragment(R.id.nav_categories);
                break;
            case BudgetWiseAppState.MODE_REGULAR_INCOME :
                switchFragment(R.id.nav_regular_income);
                break;
            case BudgetWiseAppState.MODE_BUDGETS:
                switchFragment(R.id.nav_budgets);
                break;
            case BudgetWiseAppState.MODE_BACKUP:
                switchFragment(R.id.nav_backup_restore);
                break;
        }
    }

    /**
     * On creating top menu use layout
     * @param menu Menu object
     * @return True
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }

    /**
     * On selection top menu item use the same handler as for navigation drawer menu
     * @param item Menu item
     * @return True
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onNavigationItemSelected(item);
        return true;
    }

    /**
     * On pressing back button go to the main fragment then to view part of the main frame then to the start screen and exit
     * Works depending on the current app state
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //close nav drawer
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        //close amount set
        else if (budgetWiseAppState.editAmountMode){
            AmountFragment amountFragment  = (AmountFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentAmount);
            amountFragment.hideFragment();
        }
        //close add/edit/delete box OR go to start screen
        else {
            switch (budgetWiseAppState.mainMode){
                case BudgetWiseAppState.MODE_SPENDING:
                    if (budgetWiseAppState.spendingOperationFlag != BudgetWiseAppState.SPENDING_NO_OPERATION){
                        spendingFragment.hideSpendingEditBox(findViewById(R.id.buttonSpendingEditCancel));
                    } else {
                        goToStartScreen();
                    }
                    break;
                case BudgetWiseAppState.MODE_CATEGORIES:
                    if (budgetWiseAppState.categoryOperationFlag != BudgetWiseAppState.CATEGORY_NO_OPERATION){
                        categoriesFragment.hideCategoryEditBox(findViewById(R.id.buttonCategoryEditCancel));
                    } else {
                        goToStartScreen();
                    }
                    break;
                case BudgetWiseAppState.MODE_INCOME:
                    if (budgetWiseAppState.incomeOperationFlag != BudgetWiseAppState.INCOME_NO_OPERATION){
                        incomeFragment.hideIncomeEditBox(findViewById(R.id.buttonIncomeEditCancel));
                    } else {
                        goToStartScreen();
                    }
                    break;
                case BudgetWiseAppState.MODE_REGULAR_INCOME:
                    if (budgetWiseAppState.regularIncomeOperationFlag != BudgetWiseAppState.REGULARINCOME_NO_OPERATION){
                        regularIncomeFragment.hideRegularIncomeEditBox(findViewById(R.id.buttonRegularIncomeEditCancel));
                    } else {
                        goToStartScreen();
                    }
                    break;
                case BudgetWiseAppState.MODE_BUDGETS:
                    if (budgetWiseAppState.budgetOperationFlag != BudgetWiseAppState.BUDGET_NO_OPERATION){
                        budgetsFragment.hideBudgetEditBox(findViewById(R.id.buttonBudgetEditCancel));
                    } else {
                        goToStartScreen();
                    }
                    break;
                case BudgetWiseAppState.MODE_TOTALS:
                    goToStartScreen();
                    break;
                case BudgetWiseAppState.MODE_BACKUP:
                    goToStartScreen();
                    break;
                //OR default action (exit app)
                default:
                    super.onBackPressed();
            }
        }
    }

    /**
     * On selection nav drawer menu item got to chosen mode fragment
     * @param item Menu item
     * @return True
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //show fragment for chosen mode
        int itemId = item.getItemId();
        switchFragment(itemId);

        //close nav drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    /**
     * Swith fragment in the main container
     * @param itemId Id of the menu item clicked
     */
    protected void switchFragment(int itemId){
        // Handle navigation view item clicks here.
        FrameLayout amountLayout = (FrameLayout) findViewById(R.id.layoutAmount);
        FrameLayout mainContainer = (FrameLayout) findViewById(R.id.layoutMainContainer);
        //close keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainContainer.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        //close amount fragment
        amountLayout.setVisibility(View.GONE);
        mainContainer.setVisibility(View.VISIBLE);
        //change background to plain
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.layoutMain);
        mainLayout.setBackgroundResource(R.drawable.innermainplain);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //show fragment for chosen mode
        if (itemId == R.id.nav_expenses) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_SPENDING;
            fragmentTransaction.replace(R.id.layoutMainContainer, spendingFragment);
        } else if (itemId == R.id.nav_categories) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_CATEGORIES;
            fragmentTransaction.replace(R.id.layoutMainContainer, categoriesFragment);
        } else if (itemId == R.id.nav_income) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_INCOME;
            fragmentTransaction.replace(R.id.layoutMainContainer, incomeFragment);
        } else if (itemId == R.id.nav_regular_income) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_REGULAR_INCOME;
            fragmentTransaction.replace(R.id.layoutMainContainer, regularIncomeFragment);
        }  else if (itemId == R.id.nav_budgets) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_BUDGETS;
            fragmentTransaction.replace(R.id.layoutMainContainer, budgetsFragment);
        }  else if (itemId == R.id.nav_totals) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_TOTALS;
            fragmentTransaction.replace(R.id.layoutMainContainer, totalsFragment);
        } else if (itemId == R.id.nav_backup_restore) {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_BACKUP;
            fragmentTransaction.replace(R.id.layoutMainContainer, backupRestoreFragment);
        } else {
            budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_START;
            fragmentTransaction.replace(R.id.layoutMainContainer, startFragment);
        }
            fragmentTransaction.commit();
    }

    //***********************************************************************************************************************

    /**
     * On dialog creation return dialog (with content) according to chosen dialog type
     * @param id Dialog type ID
     * @return Dialog
     */
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            return datePickerDialog;
        }
        return super.onCreateDialog(id);
    }

    /**
     * On clicking date field set mode for date picker dialog to put date text to
     * @param view Date edit field
     */
    public void onDateEditClick(View view) {
        //set mode
        switch (view.getId()){
            case R.id.editTextSpendingDate:
                budgetWiseAppState.currentDatePickerMode = BudgetWiseAppState.DATEPICKER_SPENDING;
                break;
            case R.id.editTextIncomeDate:
                budgetWiseAppState.currentDatePickerMode = BudgetWiseAppState.DATEPICKER_INCOME;
                break;
            case R.id.editTextRegularIncomeStartDate:
                budgetWiseAppState.currentDatePickerMode = BudgetWiseAppState.DATEPICKER_REGULAR_INCOME;
                break;
        }
        //show dialog
        datePickerDialog.getDatePicker().init(budgetWiseAppState.operationalYear, budgetWiseAppState.operationalMonth, budgetWiseAppState.operationalDay, null);
        showDialog(DIALOG_DATE);
    }

    /**
     * On clicking amount field
     * @param view Amount edit field
     */
    public void onAmountEditClick(View view) {
        FrameLayout amountContainer = (FrameLayout) findViewById(R.id.layoutAmount);
        FrameLayout mainContainer = (FrameLayout) findViewById(R.id.layoutMainContainer);
        //set target field
        budgetWiseAppState.amountTarget = view.getId();
        EditText editText = (EditText) findViewById(view.getId());
        //get number from the field
        String amountStr = editText.getText().toString().trim();
        BigDecimal targetAmount;
        if (amountStr.length() > 0){
            targetAmount = new BigDecimal(amountStr);
        } else {
            targetAmount = new BigDecimal("0");
        }
        //set operational int value for the amount fragment operations
        targetAmount = targetAmount.multiply(new BigDecimal("100"));
        budgetWiseAppState.currentAmount = targetAmount.intValue();
        //set amount edit mode
        switch (view.getId()){
            case R.id.editTextSpendingAmount:
                budgetWiseAppState.amountMode = BudgetWiseAppState.AMOUNT_CALCULATE;
                break;
            case R.id.editTextIncomeAmount:
            case R.id.editTextRegularIncomeAmount:
            case R.id.editTextBudgetAmount:
                budgetWiseAppState.amountMode = BudgetWiseAppState.AMOUNT_SET;
                break;
        }
        //initialise fragment
        AmountFragment amountFragment  = (AmountFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentAmount);
        amountFragment.setState(budgetWiseAppState.amountMode);
        //close keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        //se app state
        budgetWiseAppState.editAmountMode = true;
        //show fragment
        mainContainer.setVisibility(View.GONE);
        amountContainer.setVisibility(View.VISIBLE);
    }

    //***********************************************************************************************************************

    /**
     * Set current date (to app state object)
     */
    protected void setCurrentDate(){
        Date date = new Date();
        Calendar calendar = MainActivity.this.calendar;
        calendar.setTime(date);
        budgetWiseAppState.currentDate = date;
        budgetWiseAppState.operationalYear = calendar.get(Calendar.YEAR);
        budgetWiseAppState.operationalMonth = calendar.get(Calendar.MONTH);
        budgetWiseAppState.operationalDay = calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Set operational date parameters (to app state object)
     * @param dateStr Date in string format (from DB)
     */
    protected void setDate(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateStr);
            Calendar calendar = MainActivity.this.calendar;
            calendar.setTime(date);
            budgetWiseAppState.operationalYear = calendar.get(Calendar.YEAR);
            budgetWiseAppState.operationalMonth = calendar.get(Calendar.MONTH);
            budgetWiseAppState.operationalDay = calendar.get(Calendar.DAY_OF_MONTH);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get date in string format (for DB)
     * @param date Date
     * @return String representation
     */
    protected String getDateStr(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = MainActivity.this.calendar;
        calendar.setTime(date);
        return (format.format(calendar.getTime()));
    }

    /**
     * Get current app state date in string format (for DB)
     * @return String representation
     */
    protected String getCurrentDateStr(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = MainActivity.this.calendar;
        calendar.set(budgetWiseAppState.operationalYear, budgetWiseAppState.operationalMonth, budgetWiseAppState.operationalDay);
        return (format.format(calendar.getTime()));
    }

    /**
     * Get current app state date in string format "for eyes" (for date field)
     * @return String representation
     */
    protected String getCurrentLocaleDateStr(){
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        Calendar calendar = MainActivity.this.calendar;
        calendar.set(budgetWiseAppState.operationalYear, budgetWiseAppState.operationalMonth, budgetWiseAppState.operationalDay);
        return (format.format(calendar.getTime()));
    }

    /**
     * Get string represented date (from DB) in string format "for eyes" (for date field)
     * @param dateStr String representation
     * @return String representation "for eyes"
     */
    protected String dateStrToLocaleDateStr(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateStr);
            Calendar calendar = MainActivity.this.calendar;
            calendar.setTime(date);
            format.applyPattern("dd MMM yyyy");
            return (format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    //***********************************************************************************************************************

    /**
     * Automatically create Income records (on app start)
     */
    protected void fillAutomaticIncomes(){
        IncomesFiller filler = new IncomesFiller();
        filler.execute();
    }

    //***********************************************************************************************************************

    /**
     * Go to app start screen from any mode
     */
    private void goToStartScreen(){
        FrameLayout mainContainer = (FrameLayout) findViewById(R.id.layoutMainContainer);
        mainContainer.removeAllViews();
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.layoutMain);
        budgetWiseAppState.mainMode = BudgetWiseAppState.MODE_START;
        switchFragment(-1);
        mainLayout.setBackgroundResource(R.drawable.main);
    }
}