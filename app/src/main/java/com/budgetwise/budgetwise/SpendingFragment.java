package com.budgetwise.budgetwise;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.SpendingRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.CategorySelectionRec;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Fragment used to manage spanding (view list, add, edit, delete)
 */
public class SpendingFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;

    /**
     * Async task to refresh the list of spending
     */
    protected class SpendingListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            SpendingArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of income for the given period from DB, pass it to the on post function
         * @param objects Parameters: link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            String startDate = "";
            String endDate = "";
            if (objects[0] != null) startDate = (String) objects[0];
            if (objects[1] != null) endDate = (String) objects[1];
            ArrayList<SpendingRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getSpendingList(startDate, endDate);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Spending", Toast.LENGTH_LONG).show();
                    }
                });
            }
            SpendingArrayAdapter adapter = new SpendingArrayAdapter(activity, data);
            ro.listView = (ListView) objects[2];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to refresh the list of spending record categories
     */
    protected class SpendingCategoriesListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            SelectedCategoriesArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of categories (with selection marks) from DB, pass it to the on post function
         * @param objects Parameters: link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            int spendingId = (Integer) objects[1];
            ArrayList<CategorySelectionRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getSpendingSelectedCategories(spendingId);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Spending Categories", Toast.LENGTH_LONG).show();
                    }
                });
            }
            SelectedCategoriesArrayAdapter adapter = new SelectedCategoriesArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to change spending records (add, edit, delete)
     */
    protected class SpendingChanger extends AsyncTask{
        //possible actions
        protected static final int ADD = 0, UPDATE = 1, DELETE = 2;

        /**
         * On post execute refresh spending list
         * @param o List view to refresh
         */
        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                String startDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
                String endDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);
                refreshSpendingList((ListView) o, startDate, endDate);
            }
        }

        /**
         * Main function. Perform changing action
         * @param objects Parameters: action type, id, date, amount, comment, selected categories, link to list view
         * @return List view to refresh
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            int actionType = (Integer) objects[0];
            int id = 0;
            if (objects[1] != null) id = (Integer) objects[1];
            String date = "";
            if (objects[2] != null) date = (String) objects[2];
            BigDecimal amount = new BigDecimal("0");
            if (objects[3] != null) amount = (BigDecimal) objects[3];
            String comment = "";
            if (objects[4] != null) comment = (String) objects[4];
            ArrayList<CategorySelectionRec> categories = new ArrayList<>();
            if (objects[5] != null) categories = (ArrayList<CategorySelectionRec>) objects[5];

            try {
                switch (actionType){
                    case ADD:
                        int lastId = databaseHelper.addSpending(date, amount, comment);
                        databaseHelper.setSpendingCategories(lastId, categories);
                        break;
                    case UPDATE:
                        databaseHelper.updateSpending(id, date, amount, comment);
                        databaseHelper.setSpendingCategories(id, categories);
                        break;
                    case DELETE:
                        databaseHelper.deleteSpending(id);
                        break;
                }
            } catch (Exception e) {
                final String message;
                switch (actionType){
                    case ADD:
                        message = "Error! Can't add Spending";
                        break;
                    case UPDATE:
                        message = "Error! Can't edit Spending";
                        break;
                    case DELETE:
                        message = "Error! Can't delete Spending";
                        break;
                    default:
                        message = "Error! Unknown Spending operation";
                        break;
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            return objects[6];
        }
    }

    /**
     * Adapter to fill spending list view with rows
     */
    protected class SpendingArrayAdapter extends ArrayAdapter<SpendingRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<SpendingRec> values;

        /**
         * Listener for delete button click
         */
        protected final View.OnClickListener btnDelSpendingRecClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.spendingOperationFlag = MainActivity.BudgetWiseAppState.SPENDING_DELETE;
                ListView listView = (ListView) activity.findViewById(R.id.listViewExpensesRecs);
                int position = listView.getPositionForView(v);
                showSpendingEditBox(position);
            }
        };

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public SpendingArrayAdapter(Context context, ArrayList<SpendingRec> values) {
            super(context, R.layout.row_layout_spending, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_spending" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_spending, parent, false);
            TextView textViewRecDate = (TextView) rowView.findViewById(R.id.textViewExpensesRecDate);
            TextView textViewRecCategory = (TextView) rowView.findViewById(R.id.textViewExpensesRecCategory);
            TextView textViewRecAmount = (TextView) rowView.findViewById(R.id.textViewExpensesRecAmount);

            //get categories string
            ArrayList<BudgetWiseDatabaseHelper.CategoryRec> categories = values.get(position).categories;
            String categoriesString = "";
            if (categories.size() > 0) {
                for (int i = 0; i < categories.size() - 1; i++) {
                    categoriesString = categoriesString + categories.get(i).name + ", ";
                }
                categoriesString = categoriesString + categories.get(categories.size() - 1).name;
            }

            //set text
            textViewRecDate.setText(activity.dateStrToLocaleDateStr(values.get(position).date));
            textViewRecCategory.setText(categoriesString);
            textViewRecAmount.setText(values.get(position).amount.toString());

            //assign del button click listener
            ImageButton btnDelCategory = (ImageButton) rowView.findViewById(R.id.imageButtonDeleteExpensesRec);
            btnDelCategory.setOnClickListener(btnDelSpendingRecClick);

            return rowView;
        }
    }

    /**
     * Listener for spending list item clicked
     */
    protected class SpendingItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Show budget edit box, pass parameters
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            SpendingArrayAdapter adapter = (SpendingArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.spendingOperationFlag = MainActivity.BudgetWiseAppState.SPENDING_EDIT;
            showSpendingEditBox(position);
        }
    }

    /**
     * Adapter to fill spending record categories list view with rows
     */
    protected class SelectedCategoriesArrayAdapter extends ArrayAdapter<CategorySelectionRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<CategorySelectionRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public SelectedCategoriesArrayAdapter(Context context, ArrayList<CategorySelectionRec> values) {
            super(context, R.layout.row_select_categories, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_select_categories, parent, false);
            //fill text
            TextView textViewName = (TextView) rowView.findViewById(R.id.textViewCategoryName);
            textViewName.setText(values.get(position).name);
            //set checkbox
            CheckBox checkBoxSelected = (CheckBox) rowView.findViewById(R.id.checkBoxCaregorySelected);
            checkBoxSelected.setChecked(values.get(position).selected);

            return rowView;
        }
    }

    /**
     * Listener for spending record categories list item clicked
     */
    protected class SpendingCategoryItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Select/unselect category in the list
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            SelectedCategoriesArrayAdapter adapter = (SelectedCategoriesArrayAdapter) parent.getAdapter();
            adapter.values.get(position).selected = !adapter.values.get(position).selected;
            CheckBox checkBoxSelected = (CheckBox) itemClicked.findViewById(R.id.checkBoxCaregorySelected);
            checkBoxSelected.setChecked(adapter.values.get(position).selected);
        }
    }

    /**
     * On creating fragment refresh main list, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        final View view = inflater.inflate(R.layout.fragment_spending, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.listViewExpensesRecs);
        String startDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
        String endDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);
        listView.setOnItemClickListener(new SpendingItemClickListener());
        refreshSpendingList(listView, startDate, endDate);
        ListView listViewCategories = (ListView) view.findViewById(R.id.listViewSpendingCategories);
        listViewCategories.setOnItemClickListener(new SpendingCategoryItemClickListener());

        //on clicking add button show edit box
        View.OnClickListener btnAddSpendingRecClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.spendingOperationFlag = MainActivity.BudgetWiseAppState.SPENDING_ADD;
                showSpendingEditBox(0);
            }
        };
        Button btnAddSpending = (Button) view.findViewById(R.id.buttonAddSpendingRec);
        btnAddSpending.setOnClickListener(btnAddSpendingRecClick);

        //on clicking Cancel button in edit box hide edit box
        View.OnClickListener btnEditSpendingCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSpendingEditBox(v);
            }
        };
        Button btnEditSpendingCancel = (Button) view.findViewById(R.id.buttonSpendingEditCancel);
        btnEditSpendingCancel.setOnClickListener(btnEditSpendingCancelClick);

        //on clicking OK button in edit box perform action, hide edit box
        View.OnClickListener btnEditSpendingOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText spendingAmountEdit = (EditText) view.findViewById(R.id.editTextSpendingAmount);
                EditText spendingCommentEdit = (EditText) view.findViewById(R.id.editTextSpendingComment);
                ListView listView = (ListView) view.findViewById(R.id.listViewExpensesRecs);
                ListView listViewCategories = (ListView) view.findViewById(R.id.listViewSpendingCategories);
                SelectedCategoriesArrayAdapter categoriesAdapter = (SelectedCategoriesArrayAdapter) listViewCategories.getAdapter();
                ArrayList<CategorySelectionRec> categories = categoriesAdapter.values;
                //get parameters
                BigDecimal amount;
                if (spendingAmountEdit.getText().length() > 0) {
                    amount = new BigDecimal(spendingAmountEdit.getText().toString());
                } else amount = new BigDecimal("0");
                String date = activity.getCurrentDateStr();
                String comment = spendingCommentEdit.getText().toString().trim();
                //perform action and hide box
                //if date is empty or amount=0 do nothing
                if (((date.length() != 0) && (amount.compareTo(new BigDecimal("0")) == 1)) || (activity.budgetWiseAppState.spendingOperationFlag == MainActivity.BudgetWiseAppState.SPENDING_DELETE)) {
                    SpendingChanger changer = new SpendingChanger();
                    switch (activity.budgetWiseAppState.spendingOperationFlag) {
                        case MainActivity.BudgetWiseAppState.SPENDING_ADD:
                            changer.execute(SpendingChanger.ADD, null, date, amount, comment, categories, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.SPENDING_EDIT:
                            changer.execute(SpendingChanger.UPDATE, activity.budgetWiseAppState.currentSpendingId, date, amount, comment, categories, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.SPENDING_DELETE:
                            changer.execute(SpendingChanger.DELETE, activity.budgetWiseAppState.currentSpendingId, null, null, null, null, listView);
                            break;
                    }
                    hideSpendingEditBox(v);
                }

            }
        };
        Button btnEditSpendingOK = (Button) view.findViewById(R.id.buttonSpendingEditOK);
        btnEditSpendingOK.setOnClickListener(btnEditSpendingOkClick);

        return view;
    }

    /**
     * Refresh spending list
     * @param listView List view
     */
    protected void refreshSpendingList(ListView listView, String startDate, String endDate){
        SpendingListRefresher refresher = new SpendingListRefresher();
        refresher.execute(startDate, endDate, listView);
    }

    /**
     * Refresh spending record categories list
     * @param listView List view
     */
    protected void refreshSpendingCategoriesList(ListView listView, int spendingId){
        listView.setAdapter(null);
        SpendingCategoriesListRefresher refresher = new SpendingCategoriesListRefresher();
        refresher.execute(listView, spendingId);
    }

    /**
     * Show edit box, hide list, set operation flag, fill fields, etc
     * @param position Values list position
     */
    protected void showSpendingEditBox(int position) {
        ListView listView = (ListView) this.getView().findViewById(R.id.listViewExpensesRecs);
        SpendingArrayAdapter adapter = (SpendingArrayAdapter)  listView.getAdapter();
        TextView spendingEditText = (TextView) this.getView().findViewById(R.id.textViewSpendingEdit);
        EditText spendingAmountEdit = (EditText) this.getView().findViewById(R.id.editTextSpendingAmount);
        EditText spendingDateEdit = (EditText) this.getView().findViewById(R.id.editTextSpendingDate);
        EditText spendingCommentEdit = (EditText) this.getView().findViewById(R.id.editTextSpendingComment);
        LinearLayout layoutEditMain = (LinearLayout) this.getView().findViewById(R.id.layoutSpendingEditMain);
        LinearLayout layoutEdit = (LinearLayout) this.getView().findViewById(R.id.layoutSpendingEdit);
        LinearLayout layoutList = (LinearLayout) this.getView().findViewById(R.id.layoutSpendingList);
        ListView spendingCategoriesList = (ListView) this.getView().findViewById(R.id.listViewSpendingCategories);

        layoutList.setVisibility(View.GONE);
        //adjust edit box to action
        switch (activity.budgetWiseAppState.spendingOperationFlag){
            case MainActivity.BudgetWiseAppState.SPENDING_ADD:
                activity.budgetWiseAppState.currentSpendingId = 0;
                activity.setCurrentDate();
                spendingEditText.setText(R.string.spending_add);
                spendingAmountEdit.setText(R.string.amount_zero);
                spendingDateEdit.setText(activity.getCurrentLocaleDateStr());
                spendingCommentEdit.setText("");
                refreshSpendingCategoriesList(spendingCategoriesList, 0);
                layoutEditMain.setVisibility(View.VISIBLE);
                activity.onAmountEditClick(spendingAmountEdit);
                break;
            case MainActivity.BudgetWiseAppState.SPENDING_EDIT:
                if ((adapter != null) && (position >= 0)) {
                    activity.budgetWiseAppState.currentSpendingId = adapter.values.get(position).id;
                    activity.setDate(adapter.values.get(position).date);
                    spendingEditText.setText(R.string.spending_edit);
                    spendingAmountEdit.setText(adapter.values.get(position).amount.toString());
                    spendingDateEdit.setText(activity.getCurrentLocaleDateStr());
                    spendingCommentEdit.setText(adapter.values.get(position).comment);
                    refreshSpendingCategoriesList(spendingCategoriesList, adapter.values.get(position).id);
                    layoutEditMain.setVisibility(View.VISIBLE);
                }
                break;
            case MainActivity.BudgetWiseAppState.SPENDING_DELETE:
                activity.budgetWiseAppState.currentSpendingId = adapter.values.get(position).id;
                spendingEditText.setText(R.string.spending_delete_q);
                layoutEditMain.setVisibility(View.GONE);
                break;
        }
        layoutEdit.setVisibility(View.VISIBLE);
    }

    /**
     * Hide edit box, show list, reset operation flag, hide keyboard
     * @param v Edit box view
     */
    protected void hideSpendingEditBox(View v) {
        activity.budgetWiseAppState.spendingOperationFlag = MainActivity.BudgetWiseAppState.SPENDING_NO_OPERATION;

        LinearLayout layoutEdit = (LinearLayout) activity.findViewById(R.id.layoutSpendingEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutSpendingList);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        layoutEdit.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
    }
}
