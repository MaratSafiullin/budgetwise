package com.budgetwise.budgetwise;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.BudgetRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.CategorySelectionRec;
import java.math.BigDecimal;
import java.util.ArrayList;


/**
 * Fragment used to manage budgets (view list, add, edit, delete)
 */
public class BudgetsFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;

    /**
     * Async task to refresh the list of budgets
     */
    protected class BudgetsListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            BudgetsArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of all budgets from DB, pass it to the on post function
         * @param objects Parameters: link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            ArrayList<BudgetRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getBudgetsList(MainActivity.BudgetWiseAppState.RANGE_NONE);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Budgets", Toast.LENGTH_LONG).show();
                    }
                });
            }
            BudgetsArrayAdapter adapter = new BudgetsArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to refresh the list of budget categories
     */
    protected class BudgetCategoriesListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            SelectedCategoriesArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of categories (with selection marks) from DB, pass it to the on post function
         * @param objects Parameters: link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            int budgetId = (Integer) objects[1];
            ArrayList<CategorySelectionRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getBudgetSelectedCategories(budgetId);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Budget Categories", Toast.LENGTH_LONG).show();
                    }
                });
            }
            SelectedCategoriesArrayAdapter adapter = new SelectedCategoriesArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to change budgets (add, edit, delete)
     */
    protected class BudgetsChanger extends AsyncTask{
        //possible actions
        protected static final int ADD = 0, UPDATE = 1, DELETE = 2;

        /**
         * On post execute refresh budgets list
         * @param o List view to refresh
         */
        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                refreshBudgetsList((ListView) o);
            }
        }

        /**
         * Main function. Perform changing action
         * @param objects Parameters: action type, id, name, monthly/weekly, amount, selected categories, link to list view
         * @return List view to refresh
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            int actionType = (Integer) objects[0];
            int id = 0;
            if (objects[1] != null) id = (Integer) objects[1];
            String name = "";
            if (objects[2] != null) name = (String) objects[2];
            boolean isMonthly = false;
            if (objects[3] != null) isMonthly = (Boolean) objects[3];
            BigDecimal amount = new BigDecimal("0");
            if (objects[4] != null) amount = (BigDecimal) objects[4];
            ArrayList<CategorySelectionRec> categories = new ArrayList<>();
            if (objects[5] != null) categories = (ArrayList<CategorySelectionRec>) objects[5];

            try {
                switch (actionType){
                    case ADD:
                        int lastId = databaseHelper.addBudget(name, isMonthly, amount);
                        databaseHelper.setBudgetCategories(lastId, categories);
                        break;
                    case UPDATE:
                        databaseHelper.updateBudget(id, name, isMonthly, amount);
                        databaseHelper.setBudgetCategories(id, categories);
                        break;
                    case DELETE:
                        databaseHelper.deleteBudget(id);
                        break;
                }
            } catch (Exception e) {
                final String message;
                switch (actionType){
                    case ADD:
                        message = "Error! Can't add a Budget";
                        break;
                    case UPDATE:
                        message = "Error! Can't edit a Budget";
                        break;
                    case DELETE:
                        message = "Error! Can't delete a Budget";
                        break;
                    default:
                        message = "Error! Unknown Budget operation";
                        break;
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            return objects[6];
        }
    }

    /**
     * Adapter to fill budgets list view with rows
     */
    protected class BudgetsArrayAdapter extends ArrayAdapter<BudgetRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<BudgetRec> values;

        /**
         * Listener for delete button click
         */
        protected final View.OnClickListener btnDelBudgetClick = new View.OnClickListener() {
            /**
             * Handle button click. Show fragment edit box, pass parameters
             * @param v Button
             */
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.budgetOperationFlag = MainActivity.BudgetWiseAppState.BUDGET_DELETE;
                ListView listView = (ListView) activity.findViewById(R.id.listViewBudgets);
                int position = listView.getPositionForView(v);
                showBudgetEditBox((BudgetsArrayAdapter) listView.getAdapter(), position);
            }
        };

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public BudgetsArrayAdapter(Context context, ArrayList<BudgetRec> values) {
            super(context, R.layout.row_layout_budget, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_categories" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_budget, parent, false);
            TextView textViewBudgetName = (TextView) rowView.findViewById(R.id.textViewBudgetName);
            TextView textViewBudgetAmount = (TextView) rowView.findViewById(R.id.textViewBudgetAmount);
            //set text
            textViewBudgetName.setText(values.get(position).name);
            textViewBudgetAmount.setText(String.format("%.2f", values.get(position).amount));
            //assign del button click listener
            ImageButton btnDelBudget = (ImageButton) rowView.findViewById(R.id.imageButtonDeleteBudget);
            btnDelBudget.setOnClickListener(btnDelBudgetClick);

            return rowView;
        }
    }

    /**
     * Listener for budget list item clicked
     */
    protected class BudgetItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Show budget edit box, pass parameters
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            BudgetsArrayAdapter adapter = (BudgetsArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.budgetOperationFlag = MainActivity.BudgetWiseAppState.BUDGET_EDIT;
            showBudgetEditBox(adapter, position);
        }
    }

    /**
     * Adapter to fill budget categories list view with rows
     */
    protected class SelectedCategoriesArrayAdapter extends ArrayAdapter<CategorySelectionRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<CategorySelectionRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public SelectedCategoriesArrayAdapter(Context context, ArrayList<CategorySelectionRec> values) {
            super(context, R.layout.row_select_categories, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_select_categories, parent, false);
            //fill text
            TextView textViewName = (TextView) rowView.findViewById(R.id.textViewCategoryName);
            textViewName.setText(values.get(position).name);
            //set checkbox
            CheckBox checkBoxSelected = (CheckBox) rowView.findViewById(R.id.checkBoxCaregorySelected);
            checkBoxSelected.setChecked(values.get(position).selected);

            return rowView;
        }
    }

    /**
     * Listener for budget categories list item clicked
     */
    protected class BudgetCategoryItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Select/unselect category in the list
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            SelectedCategoriesArrayAdapter adapter = (SelectedCategoriesArrayAdapter) parent.getAdapter();
            adapter.values.get(position).selected = !adapter.values.get(position).selected;
            CheckBox checkBoxSelected = (CheckBox) itemClicked.findViewById(R.id.checkBoxCaregorySelected);
            checkBoxSelected.setChecked(adapter.values.get(position).selected);
        }
    }

    /**
     * On creating fragment refresh main list, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        final View view = inflater.inflate(R.layout.fragment_budgets, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listViewBudgets);
        ListView listViewCategories = (ListView) view.findViewById(R.id.listViewBudgetCategories);
        listView.setOnItemClickListener(new BudgetItemClickListener());
        refreshBudgetsList(listView);
        listViewCategories.setOnItemClickListener(new BudgetCategoryItemClickListener());

        //on clicking add button show edit box
        View.OnClickListener btnAddBudgetClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.budgetOperationFlag = MainActivity.BudgetWiseAppState.BUDGET_ADD;
                showBudgetEditBox(null, 0);
            }
        };
        Button btnAddBudget = (Button) view.findViewById(R.id.buttonAddBudget);
        btnAddBudget.setOnClickListener(btnAddBudgetClick);

        //on clicking Cancel button in edit box hide edit box
        View.OnClickListener btnEditBudgetCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBudgetEditBox(v);
            }
        };
        Button btnEditBudgetCancel = (Button) view.findViewById(R.id.buttonBudgetEditCancel);
        btnEditBudgetCancel.setOnClickListener(btnEditBudgetCancelClick);

        //on clicking OK button in edit box perform action, hide edit box
        View.OnClickListener btnEditBudgetOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText budgetNameEdit = (EditText) activity.findViewById(R.id.editTextBudgetName);
                EditText budgetAmountEdit = (EditText) activity.findViewById(R.id.editTextBudgetAmount);
                RadioButton radioButtonMonthly = (RadioButton) activity.findViewById(R.id.radioButtonBudgetMonthly);
                RadioButton radioButtonWeekly = (RadioButton) activity.findViewById(R.id.radioButtonBudgetWeekly);
                ListView listView = (ListView) view.findViewById(R.id.listViewBudgets);
                ListView listViewCategories = (ListView) view.findViewById(R.id.listViewBudgetCategories);
                SelectedCategoriesArrayAdapter categoriesAdapter = (SelectedCategoriesArrayAdapter) listViewCategories.getAdapter();
                ArrayList<CategorySelectionRec> categories = categoriesAdapter.values;
                //get parameters
                BigDecimal amount;
                if (budgetAmountEdit.getText().length() > 0) {
                    amount = new BigDecimal(budgetAmountEdit.getText().toString());
                } else amount = new BigDecimal("0");
                String name = budgetNameEdit.getText().toString().trim();
                boolean isMonthly = false;
                if (radioButtonMonthly.isChecked()) {
                    isMonthly = true;
                } else  if (radioButtonWeekly.isChecked()) {
                    isMonthly = false;
                }
                //perform action and hide box
                //if name is empty or amount=0 do nothing
                if (((name.length() != 0) && (amount.compareTo(new BigDecimal("0")) == 1)) || (activity.budgetWiseAppState.budgetOperationFlag == MainActivity.BudgetWiseAppState.BUDGET_DELETE)) {
                    BudgetsChanger changer = new BudgetsChanger();
                    switch (activity.budgetWiseAppState.budgetOperationFlag) {
                        case MainActivity.BudgetWiseAppState.BUDGET_ADD:
                            changer.execute(BudgetsChanger.ADD, null, name, isMonthly, amount, categories, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.BUDGET_EDIT:
                            changer.execute(BudgetsChanger.UPDATE, activity.budgetWiseAppState.currentBudgetId, name, isMonthly, amount, categories, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.BUDGET_DELETE:
                            changer.execute(BudgetsChanger.DELETE, activity.budgetWiseAppState.currentBudgetId, null, null, null, null, listView);
                            break;
                    }
                    hideBudgetEditBox(v);
                }
            }
        };
        Button btnEditBudgetOK = (Button) view.findViewById(R.id.buttonBudgetEditOK);
        btnEditBudgetOK.setOnClickListener(btnEditBudgetOkClick);

        return  view;
    }

    /**
     * Refresh budgets list
     * @param listView List view
     */
    protected void refreshBudgetsList(ListView listView){
        BudgetsListRefresher refresher = new BudgetsListRefresher();
        refresher.execute(listView);
    }

    /**
     * Refresh budget categories list
     * @param listView List view
     */
    protected void refreshBudgetCategoriesList(ListView listView, int budgetId){
        listView.setAdapter(null);
        BudgetCategoriesListRefresher refresher = new BudgetCategoriesListRefresher();
        refresher.execute(listView, budgetId);
    }

    /**
     * Show edit box, hide list, set operation flag, fill fields, etc
     * @param adapter List view adapter
     * @param position Values list position
     */
    protected void showBudgetEditBox(BudgetsArrayAdapter adapter, int position) {
        TextView budgetEditText = (TextView) activity.findViewById(R.id.textViewBudgetEdit);
        EditText budgetNameEdit = (EditText) activity.findViewById(R.id.editTextBudgetName);
        EditText budgetAmountEdit = (EditText) activity.findViewById(R.id.editTextBudgetAmount);
        RadioButton radioButtonMonthly = (RadioButton) activity.findViewById(R.id.radioButtonBudgetMonthly);
        RadioButton radioButtonWeekly = (RadioButton) activity.findViewById(R.id.radioButtonBudgetWeekly);
        LinearLayout budgetEditLayout = (LinearLayout) activity.findViewById(R.id.layoutBudgetEdit);
        LinearLayout budgetEditLayoutMain = (LinearLayout) activity.findViewById(R.id.layoutBudgetEditMain);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutBudgetsList);
        ListView budgetCategoriesList = (ListView) activity.findViewById(R.id.listViewBudgetCategories);
        //
        layoutList.setVisibility(View.GONE);
        //adjust edit box to action
        switch (activity.budgetWiseAppState.budgetOperationFlag){
            case MainActivity.BudgetWiseAppState.BUDGET_ADD:
                activity.budgetWiseAppState.currentBudgetId = 0;
                budgetEditText.setText(R.string.budget_add);
                budgetNameEdit.setText("");
                budgetAmountEdit.setText(R.string.amount_zero);
                radioButtonMonthly.setChecked(false);
                radioButtonWeekly.setChecked(true);
                refreshBudgetCategoriesList(budgetCategoriesList, 0);
                //need fields
                budgetEditLayoutMain.setVisibility(View.VISIBLE);
                //start filling from name, show keyboard
                budgetNameEdit.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(budgetNameEdit, InputMethodManager.SHOW_IMPLICIT);
                break;
            case MainActivity.BudgetWiseAppState.BUDGET_EDIT:
                if ((adapter != null) && (position >= 0)) {
                    activity.budgetWiseAppState.currentBudgetId = adapter.values.get(position).id;
                    budgetEditText.setText(R.string.budget_edit);
                    budgetNameEdit.setText(adapter.values.get(position).name);
                    budgetAmountEdit.setText(adapter.values.get(position).amount.toString());
                    if (adapter.values.get(position).isMonthly){
                        radioButtonMonthly.setChecked(true);
                        radioButtonWeekly.setChecked(false);
                    } else {
                        radioButtonMonthly.setChecked(false);
                        radioButtonWeekly.setChecked(true);
                    }
                    refreshBudgetCategoriesList(budgetCategoriesList, adapter.values.get(position).id);
                    //need fields
                    budgetEditLayoutMain.setVisibility(View.VISIBLE);
                }
                break;
            case MainActivity.BudgetWiseAppState.BUDGET_DELETE:
                activity.budgetWiseAppState.currentBudgetId = adapter.values.get(position).id;
                budgetEditText.setText(R.string.budget_delete_q);
                //don't need fields
                budgetEditLayoutMain.setVisibility(View.GONE);
                break;
        }
        //
        budgetEditLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Hide edit box, show list, reset operation flag, hide keyboard
     * @param v Edit box view
     */
    protected void hideBudgetEditBox(View v) {
        activity.budgetWiseAppState.budgetOperationFlag = MainActivity.BudgetWiseAppState.BUDGET_NO_OPERATION;

        LinearLayout layoutEdit = (LinearLayout) activity.findViewById(R.id.layoutBudgetEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutBudgetsList);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        layoutEdit.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
    }
}
