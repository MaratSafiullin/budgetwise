package com.budgetwise.budgetwise;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FilenameFilter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.CategoryRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.IncomeRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.SpendingRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.BudgetRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.RegularIncomeRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.SpendingToCategoryRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.BudgetToCategoryRec;


/**
 * A simple {@link Fragment} subclass.
 */
public class BackupRestoreFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;
    //backup folder
    private final String BACKUP_DIR = "BudgetWise/Backups";
    File backupFolder;
    String restoreFilePath;

    /**
     * Restore file record
     */
    private class RestoreRec{
        String name;
        String filePath;

        public RestoreRec(String name, String filePath) {
            this.name = name;
            this.filePath = filePath;
        }
    }

    /**
     * Adapter to fill restore files list
     */
    protected class RestoreFilesArrayAdapter extends ArrayAdapter<RestoreRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<RestoreRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public RestoreFilesArrayAdapter(Context context, ArrayList<RestoreRec> values) {
            super(context, R.layout.row_layout_simplespinner, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            TextView textViewName = (TextView) rowView;
            //set text
            textViewName.setText(values.get(position).name);

            return textViewName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            TextView textViewName = (TextView) rowView;
            //set text
            textViewName.setText(values.get(position).name);

            return textViewName;
        }
    }

    /**
     * Async task to backup database
     */
    protected class BackupPerformer extends AsyncTask {
        ProgressDialog progressDialog;
        SimpleDateFormat format;
        String filename;
        String currentTimeString;

        /**
         * Show progress dialog
         */
        @Override
        protected void onPreExecute() {
            format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currentTime = new Date();
            currentTimeString = format.format(currentTime);
            format = new SimpleDateFormat("yyyy-MM-dd_HH;mm;ss");
            filename = backupFolder.getAbsolutePath() + "/Backup_" + format.format(currentTime) + ".xml";

            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Performing Backup " + currentTimeString + "...");
            progressDialog.show();
        }

        /**
         * Hide progress dialog
         */
        @Override
        protected void onPostExecute(Object o) {
            progressDialog.dismiss();

            Spinner spinner = (Spinner) activity.findViewById(R.id.spinnerRestore);
            refreshRestoreFilesList(spinner);
        }

        /**
         * Create backup file
         * @param objects
         * @return
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            File file = new File(filename);
            try {

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                DOMImplementation impl = builder.getDOMImplementation();
                Document doc = impl.createDocument(null, null, null);

                Element rootElement = doc.createElement("BudgetWiseDatabaseBackup");
                rootElement.setAttribute("time", currentTimeString);
                doc.appendChild(rootElement);

                //categories
                ArrayList<CategoryRec> categories = databaseHelper.getCategoriesList();
                Element categoriesListElement = doc.createElement("CategoriesList");
                categoriesListElement.setAttribute("count", String.valueOf(categories.size()));
                for (int i = 0; i < categories.size(); i++) {
                    Element categoryElement = doc.createElement("Category");

                    Element id = doc.createElement("id");
                    id.setTextContent(String.valueOf(categories.get(i).id));
                    categoryElement.appendChild(id);
                    Element name = doc.createElement("name");
                    name.setTextContent(categories.get(i).name);
                    categoryElement.appendChild(name);

                    categoriesListElement.appendChild(categoryElement);
                }
                rootElement.appendChild(categoriesListElement);

                //spending
                ArrayList<SpendingRec> spending = databaseHelper.getSpendingList("", "");
                Element spendingListElement = doc.createElement("SpendingList");
                spendingListElement.setAttribute("count", String.valueOf(spending.size()));
                for (int i = 0; i < spending.size(); i++) {
                    Element spendingElement = doc.createElement("Spending");

                    Element id = doc.createElement("id");
                    id.setTextContent(String.valueOf(spending.get(i).id));
                    spendingElement.appendChild(id);
                    Element date = doc.createElement("date");
                    date.setTextContent(spending.get(i).date);
                    spendingElement.appendChild(date);
                    Element amount = doc.createElement("amount");
                    amount.setTextContent(spending.get(i).amount.toString());
                    spendingElement.appendChild(amount);
                    Element comment = doc.createElement("comment");
                    comment.setTextContent(spending.get(i).comment);
                    spendingElement.appendChild(comment);

                    spendingListElement.appendChild(spendingElement);
                }
                rootElement.appendChild(spendingListElement);

                //regular income
                ArrayList<RegularIncomeRec> regularIncomes = databaseHelper.getRegularIncomesList();
                Element regularIncomeListElement = doc.createElement("RegularIncomeList");
                regularIncomeListElement.setAttribute("count", String.valueOf(regularIncomes.size()));
                for (int i = 0; i < regularIncomes.size(); i++) {
                    Element spendingElement = doc.createElement("RegularIncomeSource");

                    Element id = doc.createElement("id");
                    id.setTextContent(String.valueOf(regularIncomes.get(i).id));
                    spendingElement.appendChild(id);
                    Element name = doc.createElement("name");
                    name.setTextContent(regularIncomes.get(i).name);
                    spendingElement.appendChild(name);
                    Element isMonthly = doc.createElement("isMonthly");
                    isMonthly.setTextContent(String.valueOf(regularIncomes.get(i).isMonthly));
                    spendingElement.appendChild(isMonthly);
                    Element dayNum = doc.createElement("dayNum");
                    dayNum.setTextContent(String.valueOf(regularIncomes.get(i).dayNum));
                    spendingElement.appendChild(dayNum);
                    Element startDate = doc.createElement("startDate");
                    startDate.setTextContent(regularIncomes.get(i).startDate);
                    spendingElement.appendChild(startDate);
                    Element lastIncomeDate = doc.createElement("lastIncomeDate");
                    lastIncomeDate.setTextContent(regularIncomes.get(i).lastIncomeDate);
                    spendingElement.appendChild(lastIncomeDate);
                    Element amount = doc.createElement("amount");
                    amount.setTextContent(regularIncomes.get(i).amount.toString());
                    spendingElement.appendChild(amount);

                    regularIncomeListElement.appendChild(spendingElement);
                }
                rootElement.appendChild(regularIncomeListElement);

                //income
                ArrayList<IncomeRec> income = databaseHelper.getIncomeList("", "");
                Element incomeListElement = doc.createElement("IncomeList");
                incomeListElement.setAttribute("count", String.valueOf(income.size()));
                for (int i = 0; i < income.size(); i++) {
                    Element incomeElement = doc.createElement("Income");

                    Element id = doc.createElement("id");
                    id.setTextContent(String.valueOf(income.get(i).id));
                    incomeElement.appendChild(id);
                    Element date = doc.createElement("date");
                    date.setTextContent(income.get(i).date);
                    incomeElement.appendChild(date);
                    Element amount = doc.createElement("amount");
                    amount.setTextContent(income.get(i).amount.toString());
                    incomeElement.appendChild(amount);
                    Element comment = doc.createElement("comment");
                    comment.setTextContent(income.get(i).comment);
                    incomeElement.appendChild(comment);
                    Element regularIncomeId = doc.createElement("regularIncomeId");
                    regularIncomeId.setTextContent(String.valueOf(income.get(i).regularIncomeId));
                    incomeElement.appendChild(regularIncomeId);

                    incomeListElement.appendChild(incomeElement);
                }
                rootElement.appendChild(incomeListElement);

                //budgets
                ArrayList<BudgetRec> budgets = databaseHelper.getBudgetsList(MainActivity.BudgetWiseAppState.RANGE_NONE);
                Element budgetsListElement = doc.createElement("BudgetsList");
                budgetsListElement.setAttribute("count", String.valueOf(budgets.size()));
                for (int i = 0; i < budgets.size(); i++) {
                    Element budgetElement = doc.createElement("Budget");

                    Element id = doc.createElement("id");
                    id.setTextContent(String.valueOf(budgets.get(i).id));
                    budgetElement.appendChild(id);
                    Element name = doc.createElement("name");
                    name.setTextContent(budgets.get(i).name);
                    budgetElement.appendChild(name);
                    Element isMonthly = doc.createElement("isMonthly");
                    isMonthly.setTextContent(String.valueOf(budgets.get(i).isMonthly));
                    budgetElement.appendChild(isMonthly);
                    Element amount = doc.createElement("amount");
                    amount.setTextContent(budgets.get(i).amount.toString());
                    budgetElement.appendChild(amount);

                    budgetsListElement.appendChild(budgetElement);
                }
                rootElement.appendChild(budgetsListElement);

                //spending to category
                ArrayList<SpendingToCategoryRec> spendingToCategory = databaseHelper.getSpendingToCategories();
                Element spendingToCategoriesElement = doc.createElement("SpendingToCategories");
                spendingToCategoriesElement.setAttribute("count", String.valueOf(spendingToCategory.size()));
                for (int i = 0; i < spendingToCategory.size(); i++) {
                    Element spendingToCategoryElement = doc.createElement("SpendingToCategoryRecord");

                    Element spendingId = doc.createElement("spendingId");
                    spendingId.setTextContent(String.valueOf(spendingToCategory.get(i).spendingId));
                    spendingToCategoryElement.appendChild(spendingId);
                    Element categoryId = doc.createElement("categoryId");
                    categoryId.setTextContent(String.valueOf(spendingToCategory.get(i).categoryId));
                    spendingToCategoryElement.appendChild(categoryId);

                    spendingToCategoriesElement.appendChild(spendingToCategoryElement);
                }
                rootElement.appendChild(spendingToCategoriesElement);

                //budget to category
                ArrayList<BudgetToCategoryRec> budgetToCategory = databaseHelper.getBudgetsToCategories();
                Element budgetsToCategoriesElement = doc.createElement("BudgetsToCategories");
                budgetsToCategoriesElement.setAttribute("count", String.valueOf(budgetToCategory.size()));
                for (int i = 0; i < budgetToCategory.size(); i++) {
                    Element budgetToCategoryElement = doc.createElement("BudgetToCategoryRecord");

                    Element budgetId = doc.createElement("budgetId");
                    budgetId.setTextContent(String.valueOf(budgetToCategory.get(i).budgetId));
                    budgetToCategoryElement.appendChild(budgetId);
                    Element categoryId = doc.createElement("categoryId");
                    categoryId.setTextContent(String.valueOf(budgetToCategory.get(i).categoryId));
                    budgetToCategoryElement.appendChild(categoryId);

                    budgetsToCategoriesElement.appendChild(budgetToCategoryElement);
                }
                rootElement.appendChild(budgetsToCategoriesElement);

                TransformerFactory tFactory = TransformerFactory.newInstance();
                Transformer transformer = tFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(file);

                transformer.transform(source, result);

            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't create backup file", Toast.LENGTH_LONG).show();
                    }
                });
                file.delete();
            }

            return null;
        }
    }

    /**
     * Async task to restore database
     */
    protected class RestorePerformer extends AsyncTask {
        ProgressDialog progressDialog;

        /**
         * Show progress dialog
         */
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Performing Restore...");
            progressDialog.show();
        }

        /**
         * Hide progress dialog
         */
        @Override
        protected void onPostExecute(Object o) {
            progressDialog.dismiss();

            activity.fillAutomaticIncomes();
        }

        /**
         * Restore database from a backup file
         * @param objects
         * @return
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            String filePath = (String) objects[0];

            try {
                databaseHelper.clearDatabase();

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(new File(filePath));
                Node rootNode = doc.getChildNodes().item(0);

                NodeList tables = rootNode.getChildNodes();
                for (int i = 0; i < tables.getLength(); i++) {
                    Node tableNode = tables.item(i);
                    NodeList records;
                    switch (tableNode.getNodeName().trim()){
                        case "CategoriesList":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("Category")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int id = 0;
                                    String name = "";
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "id":
                                                id = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "name":
                                                name = fieldNode.getTextContent();
                                                break;
                                        }
                                    }
                                    databaseHelper.addCategoryFromBackup(id, name);
                                }
                            }
                            break;
                        case "SpendingList":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("Spending")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int id = 0;
                                    String date = "";
                                    BigDecimal amount = null;
                                    String comment = "";
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "id":
                                                id = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "date":
                                                date = fieldNode.getTextContent();
                                                break;
                                            case "amount":
                                                amount = new BigDecimal(fieldNode.getTextContent());
                                                break;
                                            case "comment":
                                                comment = fieldNode.getTextContent();
                                                break;
                                        }
                                    }
                                    databaseHelper.addSpendingFromBackup(id, date, amount, comment);
                                }
                            }
                            break;
                        case "RegularIncomeList":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("RegularIncomeSource")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int id = 0;
                                    String name = "";
                                    boolean isMonthly = false;
                                    int dayNum = 0;
                                    String startDate = "";
                                    String lastIncomeDate = "";
                                    BigDecimal amount = null;
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "id":
                                                id = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "name":
                                                name = fieldNode.getTextContent();
                                                break;
                                            case "isMonthly":
                                                isMonthly = Boolean.parseBoolean(fieldNode.getTextContent());
                                                break;
                                            case "startDate":
                                                startDate = fieldNode.getTextContent();
                                                break;
                                            case "lastIncomeDate":
                                                lastIncomeDate = fieldNode.getTextContent();
                                                break;
                                            case "amount":
                                                amount = new BigDecimal(fieldNode.getTextContent());
                                                break;
                                        }
                                    }
                                    databaseHelper.addRegularIncomeFromBackup(id, name, isMonthly, dayNum, startDate, lastIncomeDate, amount);
                                }
                            }
                            break;
                        case "IncomeList":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("Income")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int id = 0;
                                    String date = "";
                                    BigDecimal amount = null;
                                    String comment = "";
                                    int regularIncomeId = 0;
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "id":
                                                id = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "date":
                                                date = fieldNode.getTextContent();
                                                break;
                                            case "amount":
                                                amount = new BigDecimal(fieldNode.getTextContent());
                                                break;
                                            case "comment":
                                                comment = fieldNode.getTextContent();
                                                break;
                                            case "regularIncomeId":
                                                regularIncomeId = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                        }
                                    }
                                    databaseHelper.addIncomeFromBackup(id, date, amount, comment, regularIncomeId);
                                }
                            }
                            break;
                        case "BudgetsList":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("Budget")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int id = 0;
                                    String name = "";
                                    boolean isMonthly = false;
                                    BigDecimal amount = null;
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "id":
                                                id = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "name":
                                                name = fieldNode.getTextContent();
                                                break;
                                            case "isMonthly":
                                                isMonthly = Boolean.parseBoolean(fieldNode.getTextContent());
                                                break;
                                            case "amount":
                                                amount = new BigDecimal(fieldNode.getTextContent());
                                                break;
                                        }
                                    }
                                    databaseHelper.addBudgetFromBackup(id, name, isMonthly, amount);
                                }
                            }
                            break;
                        case "SpendingToCategories":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("SpendingToCategoryRecord")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int spendingId = 0;
                                    int categoryId = 0;
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "spendingId":
                                                spendingId = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "categoryId":
                                                categoryId = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                        }
                                    }
                                    databaseHelper.addSpendingToCategoryRecFromBackup(spendingId, categoryId);
                                }
                            }
                            break;
                        case "BudgetsToCategories":
                            records = tableNode.getChildNodes();
                            for (int j = 0; j < records.getLength(); j++) {
                                Node recordNode = records.item(j);
                                if (recordNode.getNodeName().trim().equals("BudgetToCategoryRecord")) {
                                    NodeList fields = recordNode.getChildNodes();
                                    int budgetId = 0;
                                    int categoryId = 0;
                                    for (int k = 0; k < fields.getLength(); k++) {
                                        Node fieldNode = fields.item(k);
                                        switch (fieldNode.getNodeName()){
                                            case "budgetId":
                                                budgetId = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                            case "categoryId":
                                                categoryId = Integer.parseInt(fieldNode.getTextContent());
                                                break;
                                        }
                                    }
                                    databaseHelper.addBudgetToCategoryRecFromBackup(budgetId, categoryId);
                                }
                            }
                            break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(activity, "Error! Can't restore database", Toast.LENGTH_LONG).show();
            }
            return null;
        }
    }

    /**
     * Async task to find files to restore from
     */
    protected class RestoreListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            Spinner spinner;
            RestoreFilesArrayAdapter adapter;
        }

        /**
         * Refresh spinner
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;

            ro.spinner.setAdapter(ro.adapter);
        }

        /**
         * Find backup files
         * @param objects
         * @return
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();

            FilenameFilter filter = new FilenameFilter() {
                File f;
                public boolean accept(File dir, String name) {
                    if(name.endsWith(".xml") && name.startsWith("Backup_")) {
                        return true;
                    }
                    f = new File(dir.getAbsolutePath()+"/"+name);
                    return f.isDirectory();
                }
            };
            String[] files = backupFolder.list(filter);
            ArrayList<RestoreRec> list = new ArrayList<>();
            for (int i = 0; i < files.length; i++) {
                String fullPath = backupFolder.getAbsolutePath() + "/" + files[i];
                String name = files[i];
                name = name.substring(7, name.length() - 4);
                name = name.replace("_", " ");
                name = name.replace(";", ":");
                list.add(new RestoreRec(name, fullPath));
            }
            RestoreFilesArrayAdapter adapter = new RestoreFilesArrayAdapter(activity, list);

            ro.spinner = (Spinner) objects[0];
            ro.adapter = adapter;

            return ro;
        }
    }

    /**
     * On creating fragment assign listeners for elements, check/create backup folder
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        View view = inflater.inflate(R.layout.fragment_backup_restore, container, false);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinnerRestore);

        //check/create backup folder
        File storage = Environment.getExternalStorageDirectory();
        backupFolder = new File(storage.getAbsolutePath() + "/" + BACKUP_DIR);
        if (!backupFolder.exists()) {
            if (backupFolder.mkdirs()){
                refreshRestoreFilesList(spinner);
            } else {
                Toast.makeText(activity, "Error! Can't find or create backup folder", Toast.LENGTH_LONG).show();
            }
        } else {
            refreshRestoreFilesList(spinner);
        }

        //perform async backup
        View.OnClickListener btnBackupClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackupPerformer backupPerformer = new BackupPerformer();
                backupPerformer.execute();
            }
        };
        Button backupButton = (Button) view.findViewById(R.id.buttonBackup);
        backupButton.setOnClickListener(btnBackupClick);

        //perform async restore
        View.OnClickListener btnRestoreClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spinner = (Spinner) activity.findViewById(R.id.spinnerRestore);
                RestoreFilesArrayAdapter adapter = (RestoreFilesArrayAdapter) spinner.getAdapter();
                restoreFilePath = adapter.values.get(spinner.getSelectedItemPosition()).filePath;
                File file = new File(restoreFilePath);

                String msg = "Restore database from: " + file.getName().replace(";", ":") + " ?";
                AlertDialog alertDialog;
                alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage(msg);
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", (DialogInterface.OnClickListener) null);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                RestorePerformer restorePerformer = new RestorePerformer();
                                restorePerformer.execute(restoreFilePath);
                            }
                        }
                );
                alertDialog.show();
            }
        };
        Button restoreButton = (Button) view.findViewById(R.id.buttonRestore);
        restoreButton.setOnClickListener(btnRestoreClick);

        return view;
    }

    /**
     * Refresh restore files list
     * @param spinner Spinner
     */
    protected void refreshRestoreFilesList(Spinner spinner){
        RestoreListRefresher refresher = new RestoreListRefresher();
        refresher.execute(spinner);
    }
}
