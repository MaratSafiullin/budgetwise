package com.budgetwise.budgetwise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;


/**
 * Fragment used to switch the operational time period (month or week) to next/previous
 */
public class DateRangeFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;

    /**
     * On creating fragment update period text, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        activity = (MainActivity) getActivity();

        View view = inflater.inflate(R.layout.fragment_date_range, container, false);
        final TextView dateRangeText = (TextView) view.findViewById(R.id.textViewDateRange);
        ImageButton buttonRangeUp = (ImageButton) view.findViewById(R.id.buttonDateRangeUp);
        ImageButton buttonRangeDown = (ImageButton) view.findViewById(R.id.buttonDateRangeDown);

        //update text
        updateDateRangeText(dateRangeText);

        //set next range, update
        buttonRangeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.budgetWiseAppState.operationalDateRange.setNext();
                updateDateRangeText(dateRangeText);
                updateParentFragment();
            }
        });

        //set next range, update
        buttonRangeDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.budgetWiseAppState.operationalDateRange.setPrevious();
                updateDateRangeText(dateRangeText);
                updateParentFragment();
            }
        });

        //on clicking date range text switch range mode (week/month)
        dateRangeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (activity.budgetWiseAppState.operationalDateRange.getRangeMode()){
                    case MainActivity.BudgetWiseAppState.RANGE_MONTH:
                        activity.budgetWiseAppState.operationalDateRange.setRangeMode(MainActivity.BudgetWiseAppState.RANGE_WEEK);
                        break;
                    case MainActivity.BudgetWiseAppState.RANGE_WEEK:
                        activity.budgetWiseAppState.operationalDateRange.setRangeMode(MainActivity.BudgetWiseAppState.RANGE_MONTH);
                        break;
                }
                updateDateRangeText(dateRangeText);
                updateParentFragment();
            }
        });

        return view;
    }

    /**
     * Set new period text to display
     * @param dateRangeText Target text view
     */
    private void updateDateRangeText(TextView dateRangeText){
        SimpleDateFormat format;
        switch (activity.budgetWiseAppState.operationalDateRange.getRangeMode()){
            case MainActivity.BudgetWiseAppState.RANGE_MONTH:
                format = new SimpleDateFormat("MMM yyyy");
                dateRangeText.setText(format.format(activity.budgetWiseAppState.operationalDateRange.dateStart));
                break;
            case MainActivity.BudgetWiseAppState.RANGE_WEEK:
                format = new SimpleDateFormat("dd MMM");
                dateRangeText.setText(format.format(activity.budgetWiseAppState.operationalDateRange.dateStart) +
                " - " + format.format(activity.budgetWiseAppState.operationalDateRange.dateEnd));
                break;
        }
    }

    /**
     * Update the parent fragment content accordingly to the new period
     */
    private void updateParentFragment(){
        String startDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
        String endDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);
        ListView listView;
        switch (activity.budgetWiseAppState.mainMode){
            case MainActivity.BudgetWiseAppState.MODE_TOTALS:
                activity.totalsFragment.refreshFragment(activity.totalsFragment.getView());
                break;
            case MainActivity.BudgetWiseAppState.MODE_INCOME:
                listView = (ListView) activity.incomeFragment.getView().findViewById(R.id.listViewIncome);
                activity.incomeFragment.refreshIncomeList(listView, startDate, endDate);
                break;
            case MainActivity.BudgetWiseAppState.MODE_SPENDING:
                listView = (ListView) activity.spendingFragment.getView().findViewById(R.id.listViewExpensesRecs);
                activity.spendingFragment.refreshSpendingList(listView, startDate, endDate);
                break;
        }
    }
}
