package com.budgetwise.budgetwise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Fragment to set (money) amount. Used instead of standard numeric keyboard
 * Amount can be set directly or using simple calculations (+/-), which is useful going through bills/receipts
 */
public class AmountFragment extends Fragment {
    /**
     * Simple internal class to keep history of calculations and perform undo actions
     */
    private class HistoryRec{
        //amount
        public int amount;
        //calc sign (+/-)
        public boolean sign;

        /**
         * Constructor
         * @param amount Amount
         * @param sign Sign (+/-)
         */
        public HistoryRec(int amount, boolean sign) {
            this.amount = amount;
            this.sign = sign;
        }
    }

    //calculations history object
    private ArrayList<HistoryRec> history = new ArrayList<>();
    //link to main app activity
    private MainActivity activity;
    //operational (current) amount set by buttons in fragment
    protected int amount = 0;

    /**
     * On creating fragment assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_amount, container, false);
        //set link to activity for convenience
        this.activity = (MainActivity) getActivity();

        //on clicking digit button change amount
        View.OnClickListener btnDigitClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText amountEdit = (EditText) activity.findViewById(R.id.editTextAmount);
                int digit;
                switch (v.getId()){
                    case R.id.button0:
                        digit = 0;
                        break;
                    case R.id.button1:
                        digit = 1;
                        break;
                    case R.id.button2:
                        digit = 2;
                        break;
                    case R.id.button3:
                        digit = 3;
                        break;
                    case R.id.button4:
                        digit = 4;
                        break;
                    case R.id.button5:
                        digit = 5;
                        break;
                    case R.id.button6:
                        digit = 6;
                        break;
                    case R.id.button7:
                        digit = 7;
                        break;
                    case R.id.button8:
                        digit = 8;
                        break;
                    case R.id.button9:
                        digit = 9;
                        break;
                    default:
                        digit = -1;
                }
                if (amount < 100000000) {
                    if (digit != -1) {
                        amount = amount * 10 + digit;
                    } else if (v.getId() == R.id.button00) {
                        amount = amount * 100;
                    }
                }
                amountEdit.setText(getAmountStr(amount));
            }
        };
        Button button0 = (Button) view.findViewById(R.id.button0);
        Button button1 = (Button) view.findViewById(R.id.button1);
        Button button2 = (Button) view.findViewById(R.id.button2);
        Button button3 = (Button) view.findViewById(R.id.button3);
        Button button4 = (Button) view.findViewById(R.id.button4);
        Button button5 = (Button) view.findViewById(R.id.button5);
        Button button6 = (Button) view.findViewById(R.id.button6);
        Button button7 = (Button) view.findViewById(R.id.button7);
        Button button8 = (Button) view.findViewById(R.id.button8);
        Button button9 = (Button) view.findViewById(R.id.button9);
        Button button00 = (Button) view.findViewById(R.id.button00);
        button0.setOnClickListener(btnDigitClick);
        button1.setOnClickListener(btnDigitClick);
        button2.setOnClickListener(btnDigitClick);
        button3.setOnClickListener(btnDigitClick);
        button4.setOnClickListener(btnDigitClick);
        button5.setOnClickListener(btnDigitClick);
        button6.setOnClickListener(btnDigitClick);
        button7.setOnClickListener(btnDigitClick);
        button8.setOnClickListener(btnDigitClick);
        button9.setOnClickListener(btnDigitClick);
        button00.setOnClickListener(btnDigitClick);

        //on clicking back button change amount
        View.OnClickListener btnBackClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText amountEdit = (EditText) activity.findViewById(R.id.editTextAmount);

                amount = amount / 10;

                amountEdit.setText(getAmountStr(amount));
            }
        };
        ImageButton btnBack = (ImageButton) view.findViewById(R.id.buttonBack);
        btnBack.setOnClickListener(btnBackClick);

        //on clicking plus or minus button add amount to history and recalculate resulting amount
        View.OnClickListener btnPlusMinusClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amount > 0) {
                    EditText amountEdit = (EditText) activity.findViewById(R.id.editTextAmount);
                    EditText resultAmountEdit = (EditText) activity.findViewById(R.id.editTextResultAmount);
                    TextView historyText = (TextView) activity.findViewById(R.id.textViewCalcHistory);

                    if (v.getId() == R.id.buttonPlus) {
                        history.add(new HistoryRec(amount, true));
                    } else if (v.getId() == R.id.buttonMinus) {
                        history.add(new HistoryRec(amount, false));
                    }
                    recalculateResultAmount();
                    amount = 0;

                    historyText.setText(getHistoryStr());
                    amountEdit.setText(getAmountStr(amount));
                    resultAmountEdit.setText(getAmountStr(activity.budgetWiseAppState.currentAmount));

                    final HorizontalScrollView scrollView = (HorizontalScrollView) view.findViewById(R.id.horizontalScrollViewHistory);
                    scrollView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.setScrollX(scrollView.getMaxScrollAmount() + scrollView.getWidth());
                        }
                    }, 300);
                }
            }
        };
        Button btnPlus = (Button) view.findViewById(R.id.buttonPlus);
        btnPlus.setOnClickListener(btnPlusMinusClick);
        Button btnMinus = (Button) view.findViewById(R.id.buttonMinus);
        btnMinus.setOnClickListener(btnPlusMinusClick);

        //on clicking undo button remove the last record from history and recalculate resulting amount
        View.OnClickListener btnUndoClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText resultAmountEdit = (EditText) activity.findViewById(R.id.editTextResultAmount);
                TextView historyText = (TextView) activity.findViewById(R.id.textViewCalcHistory);

                if (history.size() > 0) {
                    history.remove(history.size() - 1);
                }

                recalculateResultAmount();

                historyText.setText(getHistoryStr());
                resultAmountEdit.setText(getAmountStr(activity.budgetWiseAppState.currentAmount));
            }
        };
        ImageButton btnUndo = (ImageButton) view.findViewById(R.id.buttonUndo);
        btnUndo.setOnClickListener(btnUndoClick);

        //on clicking OK button pass operational or resulting amount to the target field and hide fragment
        //if the amount is zero,
        View.OnClickListener btnOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int finalAmount = 0;
                if (activity.budgetWiseAppState.amountMode == MainActivity.BudgetWiseAppState.AMOUNT_SET){
                    finalAmount = amount;
                } else if (activity.budgetWiseAppState.amountMode == MainActivity.BudgetWiseAppState.AMOUNT_CALCULATE){
                    finalAmount = activity.budgetWiseAppState.currentAmount;
                    if (finalAmount == 0) finalAmount = amount;
                }
                if (finalAmount > 0) {
                    EditText editText = (EditText) activity.findViewById(activity.budgetWiseAppState.amountTarget);
                    editText.setText(getAmountStr(finalAmount));
                    hideFragment();
                }
            }
        };
        Button btnOk = (Button) view.findViewById(R.id.buttonAmountEditOK);
        btnOk.setOnClickListener(btnOkClick);

        //on clicking Cancel button hide fragment with no other actions
        View.OnClickListener btnCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFragment();
            }
        };
        Button btnCancel = (Button) view.findViewById(R.id.buttonAmountEditCancel);
        btnCancel.setOnClickListener(btnCancelClick);

        return view;
    }

    /**
     * Convert integer value in cents into a string. Used instead of formatting float for precision
     * @param amount Money amount is cents
     * @return String representation
     */
    private String getAmountStr(int amount){
        String sign = "";
        if (amount < 0){
            sign = "-";
            amount = -amount;
        }
        int amountMain = amount / 100;
        int amountCents = amount % 100;
        String strCents = String .valueOf(amountCents);
        if (strCents.length() == 1) strCents = "0" + strCents;
        return sign + amountMain + "." + strCents;
    }

    /**
     * Recalculate resulting amount based on calc history from history object records
     */
    private void recalculateResultAmount(){
        int result = 0;

        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).sign) {
                result = result + history.get(i).amount;
            } else {
                result = result - history.get(i).amount;
            }
        }

        activity.budgetWiseAppState.currentAmount = result;
    }

    /**
     * Create calculations history string to display in fragment
     * @return String representation
     */
    private String getHistoryStr(){
        String result = "";

        for (int i = 0; i < history.size(); i++) {
            String signStr;
            if (history.get(i).sign) {
                signStr = "+";
            } else {
                signStr = "-";
            }
            result = result + signStr + getAmountStr(history.get(i).amount);
        }

        return result;
    }

    /**
     * Hide amount (after clicking OK or Cancel button)
     */
    protected void hideFragment(){
        FrameLayout amountLayout = (FrameLayout) activity.findViewById(R.id.layoutAmount);
        FrameLayout mainContainer = (FrameLayout) activity.findViewById(R.id.layoutMainContainer);
        activity.budgetWiseAppState.editAmountMode = false;
        amountLayout.setVisibility(View.GONE);
        mainContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Set initial fragment state before showing (direct amount set or calculating result amount)
     * @param mode Fragment mode
     */
    public void setState(int mode){
        EditText resultAmountEdit = (EditText) activity.findViewById(R.id.editTextResultAmount);
        EditText amountEdit = (EditText) activity.findViewById(R.id.editTextAmount);
        TextView historyText = (TextView) activity.findViewById(R.id.textViewCalcHistory);
        TableRow calcRow = (TableRow) activity.findViewById(R.id.tableRowCalculating);

        //show and hide elements according to state
        //set initial values for amount and history if necessary
        switch (mode){
            case MainActivity.BudgetWiseAppState.AMOUNT_CALCULATE:
                this.amount = 0;
                this.history.clear();
                this.history.add(new HistoryRec(activity.budgetWiseAppState.currentAmount, true));
                amountEdit.setText(getAmountStr(amount));
                historyText.setText(getHistoryStr());
                resultAmountEdit.setText(getAmountStr(activity.budgetWiseAppState.currentAmount));

                resultAmountEdit.setVisibility(View.VISIBLE);
                historyText.setVisibility(View.VISIBLE);
                calcRow.setVisibility(View.VISIBLE);
                break;
            case MainActivity.BudgetWiseAppState.AMOUNT_SET:
                this.amount = activity.budgetWiseAppState.currentAmount;
                amountEdit.setText(getAmountStr(amount));

                resultAmountEdit.setVisibility(View.GONE);
                historyText.setVisibility(View.GONE);
                calcRow.setVisibility(View.GONE);
                break;
        }
    }
}
