package com.budgetwise.budgetwise;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.IncomeRec;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Fragment used to manage income (view list, add, edit, delete)
 */
public class IncomeFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;

    /**
     * Async task to refresh the list of income
     */
    protected class IncomeListRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            IncomeArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of income for the given period from DB, pass it to the on post function
         * @param objects Parameters: period start(string), period end(string), link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            String startDate = "";
            String endDate = "";
            if (objects[0] != null) startDate = (String) objects[0];
            if (objects[1] != null) endDate = (String) objects[1];
            ArrayList<IncomeRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getIncomeList(startDate, endDate);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Income", Toast.LENGTH_LONG).show();
                    }
                });
            }
            IncomeArrayAdapter adapter = new IncomeArrayAdapter(activity, data);
            ro.listView = (ListView) objects[2];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to change income records (add, edit, delete)
     */
    protected class IncomeChanger extends AsyncTask{
        //possible actions
        protected static final int ADD = 0, UPDATE = 1, DELETE = 2;

        /**
         * On post execute refresh income list
         * @param o List view to refresh
         */
        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                String startDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
                String endDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);
                refreshIncomeList((ListView) o, startDate, endDate);
            }
        }

        /**
         * Main function. Perform changing action
         * @param objects Parameters: action type, id, date, amount, comment, link to list view
         * @return List view to refresh
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            int actionType = (Integer) objects[0];
            int id = 0;
            if (objects[1] != null) id = (Integer) objects[1];
            String date = "";
            if (objects[2] != null) date = (String) objects[2];
            BigDecimal amount = new BigDecimal("0");
            if (objects[3] != null) amount = (BigDecimal) objects[3];
            String comment = "";
            if (objects[4] != null) comment = (String) objects[4];

            try {
                switch (actionType){
                    case ADD:
                        databaseHelper.addIncome(date, amount, comment, 0);
                        break;
                    case UPDATE:
                        databaseHelper.updateIncome(id, date, amount, comment);
                        break;
                    case DELETE:
                        databaseHelper.deleteIncome(id);
                        break;
                }
            } catch (Exception e) {
                final String message;
                switch (actionType){
                    case ADD:
                        message = "Error! Can't add Income";
                        break;
                    case UPDATE:
                        message = "Error! Can't edit Income";
                        break;
                    case DELETE:
                        message = "Error! Can't delete Income";
                        break;
                    default:
                        message = "Error! Unknown Income operation";
                        break;
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            return objects[5];
        }
    }

    /**
     * Adapter to fill income list view with rows
     */
    protected class IncomeArrayAdapter extends ArrayAdapter<IncomeRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<IncomeRec> values;

        /**
         * Listener for delete button click
         */
        protected final View.OnClickListener btnDelIncomeClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.incomeOperationFlag = MainActivity.BudgetWiseAppState.INCOME_DELETE;
                ListView listView = (ListView) activity.findViewById(R.id.listViewIncome);
                int position = listView.getPositionForView(v);
                showIncomeEditBox((IncomeArrayAdapter) listView.getAdapter(), position);
            }
        };

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public IncomeArrayAdapter(Context context, ArrayList<IncomeRec> values) {
            super(context, R.layout.row_layout_income, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_spending" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_income, parent, false);
            TextView textViewIncomeDate = (TextView) rowView.findViewById(R.id.textViewIncomeDate);
            TextView textViewIncomeAmount = (TextView) rowView.findViewById(R.id.textViewIncomeAmount);
            //set text
            textViewIncomeDate.setText(activity.dateStrToLocaleDateStr(values.get(position).date));
            textViewIncomeAmount.setText(values.get(position).amount.toString());
            //assign del button click listener
            ImageButton btnDelIncome = (ImageButton) rowView.findViewById(R.id.imageButtonDeleteIncome);
            btnDelIncome.setOnClickListener(btnDelIncomeClick);

            return rowView;
        }
    }

    /**
     * Listener for income list item clicked
     */
    protected class IncomeItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Show income edit box, pass parameters
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            IncomeArrayAdapter adapter = (IncomeArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.incomeOperationFlag = MainActivity.BudgetWiseAppState.INCOME_EDIT;
            showIncomeEditBox(adapter, position);
        }
    }

    /**
     * On creating fragment refresh main list, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        final View view = inflater.inflate(R.layout.fragment_income, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.listViewIncome);
        String startDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
        String endDate = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);
        listView.setOnItemClickListener(new IncomeItemClickListener());
        refreshIncomeList(listView, startDate, endDate);

        //on clicking add button show edit box
        View.OnClickListener btnAddIncomeClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.incomeOperationFlag = MainActivity.BudgetWiseAppState.INCOME_ADD;
                showIncomeEditBox(null, 0);
            }
        };
        Button btnAddIncome = (Button) view.findViewById(R.id.buttonAddIncome);
        btnAddIncome.setOnClickListener(btnAddIncomeClick);

        //on clicking Cancel button in edit box hide edit box
        View.OnClickListener btnEditIncomeCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideIncomeEditBox(v);
            }
        };
        Button btnEditIncomeCancel = (Button) view.findViewById(R.id.buttonIncomeEditCancel);
        btnEditIncomeCancel.setOnClickListener(btnEditIncomeCancelClick);

        //on clicking OK button in edit box perform action, hide edit box
        View.OnClickListener btnEditIncomeOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText incomeAmountEdit = (EditText) view.findViewById(R.id.editTextIncomeAmount);
                EditText incomeCommentEdit = (EditText) view.findViewById(R.id.editTextIncomeComment);
                ListView listView = (ListView) view.findViewById(R.id.listViewIncome);

                //get parameters
                BigDecimal amount;
                if (incomeAmountEdit.getText().length() > 0) {
                    amount = new BigDecimal(incomeAmountEdit.getText().toString());
                } else amount = new BigDecimal("0");
                String date = activity.getCurrentDateStr();
                String comment = incomeCommentEdit.getText().toString().trim();
                //perform action and hide box
                //if date is empty or amount=0 do nothing
                if (((date != "") && (amount.compareTo(new BigDecimal("0")) == 1)) || (activity.budgetWiseAppState.incomeOperationFlag == MainActivity.BudgetWiseAppState.INCOME_DELETE)) {
                    IncomeChanger changer = new IncomeChanger();
                    switch (activity.budgetWiseAppState.incomeOperationFlag) {
                        case MainActivity.BudgetWiseAppState.INCOME_ADD:
                            changer.execute(IncomeChanger.ADD, null, date, amount, comment, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.INCOME_EDIT:
                            changer.execute(IncomeChanger.UPDATE, activity.budgetWiseAppState.currentIncomeId, date, amount, comment, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.INCOME_DELETE:
                            changer.execute(IncomeChanger.DELETE, activity.budgetWiseAppState.currentIncomeId, date, amount, comment, listView);
                            break;
                    }
                    hideIncomeEditBox(v);
                }

            }
        };
        Button btnEditIncomeOK = (Button) view.findViewById(R.id.buttonIncomeEditOK);
        btnEditIncomeOK.setOnClickListener(btnEditIncomeOkClick);

        return view;
    }

    /**
     * Refresh income list
     * @param listView List view
     */
    protected void refreshIncomeList(ListView listView, String startDate, String endDate){
        IncomeListRefresher refresher = new IncomeListRefresher();
        refresher.execute(startDate, endDate, listView);
    }

    /**
     * Show edit box, hide list, set operation flag, fill fields, etc
     * @param adapter List view adapter
     * @param position Values list position
     */
    protected void showIncomeEditBox(IncomeArrayAdapter adapter, int position) {
        TextView incomeEditText = (TextView) activity.findViewById(R.id.textViewIncomeEdit);
        EditText incomeAmountEdit = (EditText) activity.findViewById(R.id.editTextIncomeAmount);
        EditText incomeDateEdit = (EditText) activity.findViewById(R.id.editTextIncomeDate);
        EditText incomeCommentEdit = (EditText) activity.findViewById(R.id.editTextIncomeComment);
        LinearLayout layoutEditMain = (LinearLayout) activity.findViewById(R.id.layoutIncomeEditMain);
        LinearLayout layoutEdit = (LinearLayout) activity.findViewById(R.id.layoutIncomeEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutIncomeList);

        layoutList.setVisibility(View.GONE);
        //adjust edit box to action
        switch (activity.budgetWiseAppState.incomeOperationFlag){
            case MainActivity.BudgetWiseAppState.INCOME_ADD:
                activity.budgetWiseAppState.currentIncomeId = 0;
                activity.setCurrentDate();
                incomeEditText.setText(R.string.income_add);
                incomeAmountEdit.setText(R.string.amount_zero);
                incomeDateEdit.setText(activity.getCurrentLocaleDateStr());
                incomeCommentEdit.setText("");
                layoutEditMain.setVisibility(View.VISIBLE);
                activity.onAmountEditClick(incomeAmountEdit);
                break;
            case MainActivity.BudgetWiseAppState.INCOME_EDIT:
                if ((adapter != null) && (position >= 0)) {
                    activity.budgetWiseAppState.currentIncomeId = adapter.values.get(position).id;
                    activity.setDate(adapter.values.get(position).date);
                    incomeEditText.setText(R.string.income_edit);
                    incomeAmountEdit.setText(adapter.values.get(position).amount.toString());
                    incomeDateEdit.setText(activity.getCurrentLocaleDateStr());
                    incomeCommentEdit.setText(adapter.values.get(position).comment);
                    layoutEditMain.setVisibility(View.VISIBLE);
                }
                break;
            case MainActivity.BudgetWiseAppState.INCOME_DELETE:
                activity.budgetWiseAppState.currentIncomeId = adapter.values.get(position).id;
                incomeEditText.setText(R.string.income_delete_q);
                layoutEditMain.setVisibility(View.GONE);
                break;
        }
        layoutEdit.setVisibility(View.VISIBLE);
    }

    /**
     * Hide edit box, show list, reset operation flag, hide keyboard
     * @param v Edit box view
     */
    protected void hideIncomeEditBox(View v) {
        activity.budgetWiseAppState.incomeOperationFlag = MainActivity.BudgetWiseAppState.INCOME_NO_OPERATION;

        LinearLayout layoutEdit = (LinearLayout) activity.findViewById(R.id.layoutIncomeEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutIncomeList);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        layoutEdit.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
    }
}
