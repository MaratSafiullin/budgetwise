package com.budgetwise.budgetwise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Start fragment used to get a brief description and to go to other fragments
 */
public class StartFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;

    /**
     * On creating fragment assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();

        View view = inflater.inflate(R.layout.fragment_start, container, false);

        //show additional links
        TextView textViewMore = (TextView) view.findViewById(R.id.textViewMore);
        textViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView textViewMore = (TextView) activity.findViewById(R.id.textViewMore);
                LinearLayout layoutMore = (LinearLayout) activity.findViewById(R.id.layoutMore);

                textViewMore.setVisibility(View.GONE);
                layoutMore.setVisibility(View.VISIBLE);
            }
        });

        //go to spending
        TextView textViewLinkSpending = (TextView) view.findViewById(R.id.textViewLinkSpending);
        textViewLinkSpending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_expenses);
            }
        });

        //go to income
        TextView textViewLinkIncome = (TextView) view.findViewById(R.id.textViewLinkIncome);
        textViewLinkIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_income);
            }
        });

        //go to totlas
        TextView textViewLinkTotals = (TextView) view.findViewById(R.id.textViewLinkTotals);
        textViewLinkTotals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_totals);
            }
        });

        //go to categories
        TextView textViewLinkCategories = (TextView) view.findViewById(R.id.textViewLinkCategories);
        textViewLinkCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_categories);
            }
        });


        //go to regular income
        TextView textViewLinkRegularIncome = (TextView) view.findViewById(R.id.textViewLinkRegularIncome);
        textViewLinkRegularIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_regular_income);
            }
        });

        //go to budgets
        TextView textViewLinkBudgets = (TextView) view.findViewById(R.id.textViewLinkBudgets);
        textViewLinkBudgets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.switchFragment(R.id.nav_budgets);
            }
        });

        return view;
    }

}
