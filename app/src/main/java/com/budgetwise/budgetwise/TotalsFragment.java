package com.budgetwise.budgetwise;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.DayRec;

import java.math.BigDecimal;
import java.util.ArrayList;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.CategoryTotalRec;
import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.BudgetTotalRec;


/**
 * Fragment used to check total results of spending and income
 */
public class TotalsFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;

    /**
     * Async task to refresh the whole fragment
     */
    protected class TotalsFragmentRefresher extends AsyncTask {
        /**
         * Result to pass from main method to post execute handler
         */
        protected class RO{
            TextView totalSpendingText;
            TextView totalIncomeText;
            TextView balanceText;
            ProgressBar totalSpendingBar;
            ProgressBar totalIncomeBar;
            BigDecimal totalSpending;
            BigDecimal totalIncome;
            BigDecimal balance;

            ListView daysList;
            TotalDaysArrayAdapter daysAdapter;

            ExpandableListView categoriesList;
            TotalCategoriesArrayAdapter categoriesAdapter;

            ExpandableListView budgetsList;
            TotalBudgetsArrayAdapter budgetsAdapter;
        }

        /**
         * On post execute refresh fragment
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            final RO ro = (RO) o;

            //refresh total spending and income part

            Handler handler = new Handler();

            ro.totalSpendingText.setText(ro.totalSpending.toString());
            ro.totalIncomeText.setText(ro.totalIncome.toString());

            final int totalSpendingInt = (int) ro.totalSpending.doubleValue() * 100;
            final int totalIncomeInt = (int) ro.totalIncome.doubleValue() * 100;
            final int maxAmount;
            if (totalIncomeInt > totalSpendingInt) {
                maxAmount = totalIncomeInt;
            } else {
                maxAmount = totalSpendingInt;
            }

            handler.post(new Runnable() {
                public void run() {
                    ro.totalSpendingBar.setMax(maxAmount);
                    ro.totalSpendingBar.setProgress(totalSpendingInt);
                    ro.totalIncomeBar.setMax(maxAmount);
                    ro.totalIncomeBar.setProgress(totalIncomeInt);
                }
            });
            ro.balanceText.setText(ro.balance.toString());
            if (ro.balance.compareTo(new BigDecimal("0")) >= 0){
                ro.balanceText.setTextColor(getResources().getColor(R.color.colorPositiveBalance));
            } else {
                ro.balanceText.setTextColor(getResources().getColor(R.color.colorNegativeBalance));
            }

            //refresh spending by date part

            ro.daysList.setAdapter(ro.daysAdapter);

            //refresh spending by category part

            ro.categoriesList.setAdapter(ro.categoriesAdapter);

            //refresh spending by budget part

            ro.budgetsList.setAdapter(ro.budgetsAdapter);
        }

        /**
         * Main function. Get totals data from DB, pass it to the on post function
         * @param objects Parameters.
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            String dateStart = "";
            String dateEnd = "";
            if (objects[0] != null) dateStart = (String) objects[0];
            if (objects[1] != null) dateEnd = (String) objects[1];

            //total

            ro.balanceText = (TextView) objects[2];
            ro.totalIncomeText = (TextView) objects[3];
            ro.totalSpendingText = (TextView) objects[4];
            ro.totalIncomeBar = (ProgressBar) objects[5];
            ro.totalSpendingBar = (ProgressBar) objects[6];

            BigDecimal totalSpending = new BigDecimal("0");
            BigDecimal totalIncome = new BigDecimal("0");
            try {
                totalSpending = databaseHelper.getTotalSpending(dateStart, dateEnd);
                totalIncome = databaseHelper.getTotalIncome(dateStart, dateEnd);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get Total Spending and/or Income", Toast.LENGTH_LONG).show();
                    }
                });
            }
            BigDecimal balance = totalIncome.subtract(totalSpending);
            ro.balance = balance;
            ro.totalSpending = totalSpending;
            ro.totalIncome = totalIncome;

            //days

            ro.daysList = (ListView) objects[7];

            ArrayList<DayRec> daysData = new ArrayList<>();
            try {
                daysData = databaseHelper.getTotalSpendingDays(dateStart, dateEnd);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get Total Spending per Day", Toast.LENGTH_LONG).show();
                    }
                });
            }
            ro.daysAdapter = new TotalDaysArrayAdapter(getActivity(), daysData);

            //categories

            ro.categoriesList = (ExpandableListView) objects[8];

            ArrayList<CategoryTotalRec> categoriesData = new ArrayList<>();
            try {
                categoriesData = databaseHelper.getTotalSpendingCategories(dateStart, dateEnd);
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get Total Spending per Category", Toast.LENGTH_LONG).show();
                    }
                });
            }
            ro.categoriesAdapter = new TotalCategoriesArrayAdapter(getActivity(), categoriesData);

            //budgets

            ro.budgetsList = (ExpandableListView) objects[9];

            ArrayList<BudgetTotalRec> budgetsData = new ArrayList<>();
            try {
                switch (activity.budgetWiseAppState.operationalDateRange.getRangeMode()){
                    case MainActivity.BudgetWiseAppState.RANGE_MONTH:
                        budgetsData = databaseHelper.getTotalSpendingBudgets(dateStart, dateEnd, true);
                        break;
                    case MainActivity.BudgetWiseAppState.RANGE_WEEK:
                        budgetsData = databaseHelper.getTotalSpendingBudgets(dateStart, dateEnd, false);
                        break;
                    default:
                        budgetsData = new ArrayList<>();
                        break;
                }
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get Total Spending per Budget", Toast.LENGTH_LONG).show();
                    }
                });
            }
            ro.budgetsAdapter = new TotalBudgetsArrayAdapter(getActivity(), budgetsData);

            //***********************************

            return ro;
        }
    }

    /**
     * Adapter to fill categories totals list view with rows (groups and group items)
     */
    public class TotalCategoriesArrayAdapter extends BaseExpandableListAdapter {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<CategoryTotalRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public TotalCategoriesArrayAdapter(Context context, ArrayList<CategoryTotalRec> values){
            this.context = context;
            this.values = values;
        }

        @Override
        public int getGroupCount() {
            return values.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return values.get(groupPosition).spending.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return values.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return values.get(groupPosition).spending.get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return values.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return values.get(groupPosition).spending.get(childPosition).id;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_group_category_total, null);
            }

            TextView textViewName = (TextView) convertView.findViewById(R.id.textViewCategoryName);
            TextView textViewAmount = (TextView) convertView.findViewById(R.id.textViewCategoryAmount);
            textViewName.setText(values.get(groupPosition).name);
            textViewAmount.setText(values.get(groupPosition).amount.toString());

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_child_category_total, null);
            }

            TextView textViewDate = (TextView) convertView.findViewById(R.id.textViewSpendingDate);
            TextView textViewAmount = (TextView) convertView.findViewById(R.id.textViewSpendingAmount);
            textViewDate.setText(values.get(groupPosition).spending.get(childPosition).date);
            textViewAmount.setText(values.get(groupPosition).spending.get(childPosition).amount.toString());

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    /**
     * Adapter to fill budgets totals list view with rows (groups and group items)
     */
    public class TotalBudgetsArrayAdapter extends BaseExpandableListAdapter {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<BudgetTotalRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public TotalBudgetsArrayAdapter(Context context, ArrayList<BudgetTotalRec> values){
            this.context = context;
            this.values = values;
        }

        @Override
        public int getGroupCount() {
            return values.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return values.get(groupPosition).spending.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return values.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return values.get(groupPosition).spending.get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return values.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return values.get(groupPosition).spending.get(childPosition).id;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_group_budget_total, null);
            }

            TextView textViewName = (TextView) convertView.findViewById(R.id.textViewBudgetName);
            TextView textViewBalance = (TextView) convertView.findViewById(R.id.textViewBudgetBalance);
            final ProgressBar progressBarBalance = (ProgressBar) convertView.findViewById(R.id.progressBarBudgetBalance);
            Handler handler = new Handler();

            textViewName.setText(values.get(groupPosition).name);
            BigDecimal actualAmount = values.get(groupPosition).actualAmount;
            BigDecimal setAmount = values.get(groupPosition).setAmount;
            String balanceText = actualAmount.toString() + " of " + setAmount.toString();
            textViewBalance.setText(balanceText);

            final int actualAmountInt = actualAmount.multiply(new BigDecimal("100")).intValue();
            final int setAmountInt = setAmount.multiply(new BigDecimal("100")).intValue();
            if (actualAmountInt <= setAmountInt){
                handler.post(new Runnable() {
                    public void run() {
                        progressBarBalance.setMax(setAmountInt);
                        progressBarBalance.setProgress(setAmountInt - actualAmountInt);
                    }
                });
                progressBarBalance.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_green));
            } else {
                handler.post(new Runnable() {
                    public void run() {
                        progressBarBalance.setMax(setAmountInt);
                        progressBarBalance.setProgress(actualAmountInt - setAmountInt);
                    }
                });
                progressBarBalance.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_red));
            }


            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_child_budget_total, null);
            }

            TextView textViewDate = (TextView) convertView.findViewById(R.id.textViewSpendingDate);
            TextView textViewAmount = (TextView) convertView.findViewById(R.id.textViewSpendingAmount);
            textViewDate.setText(values.get(groupPosition).spending.get(childPosition).date);
            textViewAmount.setText(values.get(groupPosition).spending.get(childPosition).amount.toString());

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    /**
     * Adapter to fill dates totals list view with rows (groups and group items)
     */
    protected class TotalDaysArrayAdapter extends ArrayAdapter<DayRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<DayRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public TotalDaysArrayAdapter(Context context, ArrayList<DayRec> values) {
            super(context, R.layout.row_layout_day, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_categories" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_day, parent, false);
            //Set text form array to TextViews
            TextView textViewDate = (TextView) rowView.findViewById(R.id.textViewDayDate);
            TextView textViewAmount = (TextView) rowView.findViewById(R.id.textViewDayAmount);
            textViewDate.setText(values.get(position).date);
            textViewAmount.setText(values.get(position).amount.toString());

            return rowView;
        }
    }

    /**
     * On creating fragment refresh it, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        View view = inflater.inflate(R.layout.fragment_totals, container, false);

        //refresh
        refreshFragment(view);

        //***********************************

        //button "total" click
        ImageButton buttonTotal = (ImageButton) view.findViewById(R.id.buttonTotal);
        buttonTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTotal();
            }
        });

        //button "days" click
        ImageButton buttonDays = (ImageButton) view.findViewById(R.id.buttonDays);
        buttonDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDays();
            }
        });

        //button "categories" click
        ImageButton buttonCategories = (ImageButton) view.findViewById(R.id.buttonCategories);
        buttonCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCategories();
            }
        });

        //button "bidgets" click
        ImageButton buttonBudgets = (ImageButton) view.findViewById(R.id.buttonBudgets);
        buttonBudgets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBudgets();
            }
        });

        //***********************************

        return view;
    }

    /**
     * Refresh fragment
     * @param view Fragment view
     */
    protected void refreshFragment(View view){
        String dateStart = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateStart);
        String dateEnd = activity.getDateStr(activity.budgetWiseAppState.operationalDateRange.dateEnd);

        TextView totalSpendingText = (TextView) view.findViewById(R.id.textViewAmountSpending);
        final ProgressBar totalSpendingBar = (ProgressBar) view.findViewById(R.id.progressBarSpending);
        TextView totalIncomeText = (TextView) view.findViewById(R.id.textViewAmountIncome);
        final ProgressBar totalIncomeBar = (ProgressBar) view.findViewById(R.id.progressBarIncome);
        TextView balanceText = (TextView) view.findViewById(R.id.textViewBalance);

        ListView daysList = (ListView) view.findViewById(R.id.listViewDays);

        ExpandableListView categoriesList = (ExpandableListView) view.findViewById(R.id.expandableListViewCategories);

        ExpandableListView budgetsList = (ExpandableListView) view.findViewById(R.id.expandableListViewBudgets);

        TotalsFragmentRefresher refresher = new TotalsFragmentRefresher();
        refresher.execute(dateStart, dateEnd, balanceText, totalIncomeText, totalSpendingText, totalIncomeBar, totalSpendingBar, daysList, categoriesList, budgetsList);
    }

    /**
     * Show "total" part of the frame, hide others
     */
    private void showTotal(){
        LinearLayout layoutTotal = (LinearLayout) activity.findViewById(R.id.layoutTotal);
        LinearLayout layoutDays = (LinearLayout) activity.findViewById(R.id.layoutDays);
        LinearLayout layoutCategories = (LinearLayout) activity.findViewById(R.id.layoutCategories);
        LinearLayout layoutBudgets = (LinearLayout) activity.findViewById(R.id.layoutBudgets);

        layoutTotal.setVisibility(View.VISIBLE);
        layoutDays.setVisibility(View.GONE);
        layoutCategories.setVisibility(View.GONE);
        layoutBudgets.setVisibility(View.GONE);
    }

    /**
     * Show "days" part of the frame, hide others
     */
    private void showDays(){
        LinearLayout layoutTotal = (LinearLayout) activity.findViewById(R.id.layoutTotal);
        LinearLayout layoutDays = (LinearLayout) activity.findViewById(R.id.layoutDays);
        LinearLayout layoutCategories = (LinearLayout) activity.findViewById(R.id.layoutCategories);
        LinearLayout layoutBudgets = (LinearLayout) activity.findViewById(R.id.layoutBudgets);

        layoutTotal.setVisibility(View.GONE);
        layoutDays.setVisibility(View.VISIBLE);
        layoutCategories.setVisibility(View.GONE);
        layoutBudgets.setVisibility(View.GONE);
    }

    /**
     * Show "categories" part of the frame, hide others
     */
    private void showCategories(){
        LinearLayout layoutTotal = (LinearLayout) activity.findViewById(R.id.layoutTotal);
        LinearLayout layoutDays = (LinearLayout) activity.findViewById(R.id.layoutDays);
        LinearLayout layoutCategories = (LinearLayout) activity.findViewById(R.id.layoutCategories);
        LinearLayout layoutBudgets = (LinearLayout) activity.findViewById(R.id.layoutBudgets);

        layoutTotal.setVisibility(View.GONE);
        layoutDays.setVisibility(View.GONE);
        layoutCategories.setVisibility(View.VISIBLE);
        layoutBudgets.setVisibility(View.GONE);
    }

    /**
     * Show "budgets" part of the frame, hide others
     */
    private void showBudgets(){
        LinearLayout layoutTotal = (LinearLayout) activity.findViewById(R.id.layoutTotal);
        LinearLayout layoutDays = (LinearLayout) activity.findViewById(R.id.layoutDays);
        LinearLayout layoutCategories = (LinearLayout) activity.findViewById(R.id.layoutCategories);
        LinearLayout layoutBudgets = (LinearLayout) activity.findViewById(R.id.layoutBudgets);

        layoutTotal.setVisibility(View.GONE);
        layoutDays.setVisibility(View.GONE);
        layoutCategories.setVisibility(View.GONE);
        layoutBudgets.setVisibility(View.VISIBLE);
    }
}
