package com.budgetwise.budgetwise;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.budgetwise.budgetwise.BudgetWiseDatabaseHelper.CategoryRec;

import java.util.ArrayList;


/**
 * Fragment used to manage spending categories (view list, add, edit, delete)
 */
public class CategoriesFragment extends Fragment {
    //link to main app activity
    private MainActivity activity;
    //link to DB handler
    private BudgetWiseDatabaseHelper databaseHelper;

    /**
     * Async task to refresh the list of categories
     */
    protected class CategoriesListRefresher extends AsyncTask{
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            CategoriesArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of all categories from DB, pass it to the on post function
         * @param objects Parameters: link to list view
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            ArrayList<CategoryRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getCategoriesList();
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Categories", Toast.LENGTH_LONG).show();
                    }
                });
            }
            CategoriesArrayAdapter adapter = new CategoriesArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to refresh the list of categories to replace the being deleted one
     */
    protected class ReplacementCategoriesListRefresher extends AsyncTask{
        /**
         * Result to pass from main method to post execute handler
         */
        private class RO{
            ListView listView;
            ReplacementCategoriesArrayAdapter adapter;
        }

        /**
         * On post execute refresh list view
         * @param o Result object
         */
        @Override
        protected void onPostExecute(Object o) {
            RO ro = (RO) o;
            ro.listView.setAdapter(ro.adapter);
        }

        /**
         * Main function. Get a list of all categories (except the deleted one) from DB, pass it to the on post function
         * @param objects Parameters: link to list view, deleted category id
         * @return Result object
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            RO ro = new RO();
            int categoryId = (Integer) objects[1];
            ArrayList<CategoryRec> data = new ArrayList<>();
            try {
                data = databaseHelper.getCategoriesList();
            } catch (Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, "Error! Can't get the list of Categories", Toast.LENGTH_LONG).show();
                    }
                });
            }
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).id == categoryId){
                    data.remove(i);
                    break;
                }
            }
            ReplacementCategoriesArrayAdapter adapter = new ReplacementCategoriesArrayAdapter(activity, data);
            ro.listView = (ListView) objects[0];
            ro.adapter = adapter;
            return ro;
        }
    }

    /**
     * Async task to change categories (add, edit, delete)
     */
    protected class CategoriesChanger extends AsyncTask{
        //possible actions
        protected static final int ADD = 0, UPDATE = 1, DELETE = 2;

        /**
         * On post execute refresh budgets list
         * @param o List view to refresh
         */
        @Override
        protected void onPostExecute(Object o) {
            if (o != null) {
                refreshCategoriesList((ListView) o);
            }
        }

        /**
         * Main function. Perform changing action
         * @param objects Parameters: action type, id, name, replacement id (for deleting), link to list view
         * @return List view to refresh
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            int actionType = (Integer) objects[0];
            int id = 0;
            if (objects[1] != null) id = (Integer) objects[1];
            String name = "";
            if (objects[2] != null) name = (String) objects[2];
            int replacementId = 0;
            if (objects[3] != null) replacementId = (Integer) objects[3];

            try {
                switch (actionType){
                    case ADD:
                        databaseHelper.addCategory(name);
                        break;
                    case UPDATE:
                        databaseHelper.updateCategory(id, name);
                        break;
                    case DELETE:
                        databaseHelper.deleteCategory(id, replacementId);
                        break;
                }
            } catch (Exception e) {
                final String message;
                switch (actionType){
                    case ADD:
                        message = "Error! Can't add a Category";
                        break;
                    case UPDATE:
                        message = "Error! Can't edit a Category";
                        break;
                    case DELETE:
                        message = "Error! Can't delete a Category";
                        break;
                    default:
                        message = "Error! Unknown Category operation";
                        break;
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            return objects[4];
        }
    }

    /**
     * Adapter to fill categories list view with rows
     */
    protected class CategoriesArrayAdapter extends ArrayAdapter<CategoryRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<CategoryRec> values;

        /**
         * Listener for delete button click
         */
        protected final View.OnClickListener btnDelCategoryRecClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.categoryOperationFlag = MainActivity.BudgetWiseAppState.CATEGORY_DELETE;
                ListView listView = (ListView) activity.findViewById(R.id.listViewCategoriesRecs);
                int position = listView.getPositionForView(v);
                showCategoryEditBox((CategoriesArrayAdapter) listView.getAdapter(), position);
            }
        };

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public CategoriesArrayAdapter(Context context, ArrayList<CategoryRec> values) {
            super(context, R.layout.row_layout_categories, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_categories" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_layout_categories, parent, false);
            //set text
            TextView textViewRecName = (TextView) rowView.findViewById(R.id.textViewCategoryRecName);
            textViewRecName.setText(values.get(position).name);
            //assign del button click listener
            ImageButton btnDelCategory = (ImageButton) rowView.findViewById(R.id.imageButtonDeleteCategoryRec);
            btnDelCategory.setOnClickListener(btnDelCategoryRecClick);

            return rowView;
        }
    }

    /**
     * Listener for category list item clicked
     */
    protected class CategoryItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Show category edit box, pass parameters
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            CategoriesArrayAdapter adapter = (CategoriesArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.categoryOperationFlag = MainActivity.BudgetWiseAppState.CATEGORY_EDIT;
            showCategoryEditBox(adapter, position);
        }
    }

    /**
     * Adapter to fill replacement categories list view with rows
     */
    protected class ReplacementCategoriesArrayAdapter extends ArrayAdapter<CategoryRec> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<CategoryRec> values;

        /**
         * Constructor
         * @param context Context
         * @param values List of values
         */
        public ReplacementCategoriesArrayAdapter(Context context, ArrayList<CategoryRec> values) {
            super(context, R.layout.row_select_category, values);
            this.context = context;
            this.values = values;
        }

        /**
         * Create row
         * @param position Row position
         * @param convertView
         * @param parent List view
         * @return Row view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //"row_layout_categories" is used to form a row of the ListView
            View rowView = inflater.inflate(R.layout.row_select_category, parent, false);
            //set text
            TextView textViewName = (TextView) rowView.findViewById(R.id.textViewCategoryName);
            textViewName.setText(values.get(position).name);
            RadioButton radioButtonSelected = (RadioButton) rowView.findViewById(R.id.radioButtonCategorySelected);
            //set radio button state
            if (values.get(position).id == activity.budgetWiseAppState.replacementCategoryId) {
                radioButtonSelected.setChecked(true);
            }

            return rowView;
        }
    }

    /**
     * Listener for replacement categories list item clicked
     */
    protected class ReplacementCategoryItemClickListener implements AdapterView.OnItemClickListener {
        /**
         * Select (one) category in the list
         * @param parent Adapter
         * @param itemClicked Item view
         * @param position Item position
         * @param id Item id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
            ReplacementCategoriesArrayAdapter adapter = (ReplacementCategoriesArrayAdapter) parent.getAdapter();
            activity.budgetWiseAppState.replacementCategoryId = adapter.values.get(position).id;
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * On creating fragment refresh main list, assign listeners for elements
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set links for convenience
        this.activity = (MainActivity) getActivity();
        this.databaseHelper = activity.databaseHelper;

        final View view = inflater.inflate(R.layout.fragment_categories, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listViewCategoriesRecs);
        listView.setOnItemClickListener(new CategoryItemClickListener());
        refreshCategoriesList(listView);

        ListView listViewReplacement = (ListView) view.findViewById(R.id.listViewReplacementCategory);
        listViewReplacement.setOnItemClickListener(new ReplacementCategoryItemClickListener());

        //on clicking add button show edit box
        View.OnClickListener btnAddCategoryRecClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.budgetWiseAppState.categoryOperationFlag = MainActivity.BudgetWiseAppState.CATEGORY_ADD;
                showCategoryEditBox(null, 0);
            }
        };
        Button btnAddCategory = (Button) view.findViewById(R.id.buttonAddCategoryRec);
        btnAddCategory.setOnClickListener(btnAddCategoryRecClick);

        //on clicking Cancel button in edit box hide edit box
        View.OnClickListener btnEditCategoryCancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCategoryEditBox(v);
            }
        };
        Button btnEditCategoryCancel = (Button) view.findViewById(R.id.buttonCategoryEditCancel);
        btnEditCategoryCancel.setOnClickListener(btnEditCategoryCancelClick);

        //on clicking OK button in edit box perform action, hide edit box
        View.OnClickListener btnEditCategoryOkClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText categoryNameEdit = (EditText) view.findViewById(R.id.editTextCategoryName);
                ListView listView = (ListView) view.findViewById(R.id.listViewCategoriesRecs);
                RadioButton radioButtonReplaceYes = (RadioButton) view.findViewById(R.id.radioButtonReplaceYes);

                //get parameters
                String name = categoryNameEdit.getText().toString().trim();
                //perform action and hide box
                //if name is empty do nothing
                if ((name.length() != 0) ||
                        ((activity.budgetWiseAppState.categoryOperationFlag == MainActivity.BudgetWiseAppState.CATEGORY_DELETE) &&
                                !(radioButtonReplaceYes.isChecked() && (activity.budgetWiseAppState.replacementCategoryId == 0)))) {
                    CategoriesChanger changer = new CategoriesChanger();
                    switch (activity.budgetWiseAppState.categoryOperationFlag) {
                        case MainActivity.BudgetWiseAppState.CATEGORY_ADD:
                            changer.execute(CategoriesChanger.ADD, null, name, null, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.CATEGORY_EDIT:
                            changer.execute(CategoriesChanger.UPDATE, activity.budgetWiseAppState.currentCategoryId, name, null, listView);
                            break;
                        case MainActivity.BudgetWiseAppState.CATEGORY_DELETE:
                            int replacementCategoryId;
                            if (radioButtonReplaceYes.isChecked()) {
                                replacementCategoryId = activity.budgetWiseAppState.replacementCategoryId;
                            } else {
                                replacementCategoryId = 0;
                            }
                            changer.execute(CategoriesChanger.DELETE, activity.budgetWiseAppState.currentCategoryId, null, replacementCategoryId, listView);
                            break;
                    }
                    hideCategoryEditBox(v);
                }
            }
        };
        Button btnEditCategoryOK = (Button) view.findViewById(R.id.buttonCategoryEditOK);
        btnEditCategoryOK.setOnClickListener(btnEditCategoryOkClick);

        //on choosing replacement option (yes/no) show or hide list of categories for replacement
        View.OnClickListener radioButtonSwitchReplacementClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton btnReplaceYes = (RadioButton) view.findViewById(R.id.radioButtonReplaceYes);
                RadioButton btnReplaceNo = (RadioButton) view.findViewById(R.id.radioButtonReplaceNo);
                ListView replacementList = (ListView) activity.findViewById(R.id.listViewReplacementCategory);

                if (btnReplaceYes.isChecked()){
                    replacementList.setVisibility(View.VISIBLE);
                } else if (btnReplaceNo.isChecked()){
                    replacementList.setVisibility(View.GONE);
                }
            }
        };
        RadioButton btnReplaceYes = (RadioButton) view.findViewById(R.id.radioButtonReplaceYes);
        RadioButton btnReplaceNo = (RadioButton) view.findViewById(R.id.radioButtonReplaceNo);
        btnReplaceYes.setOnClickListener(radioButtonSwitchReplacementClick);
        btnReplaceNo.setOnClickListener(radioButtonSwitchReplacementClick);

        return view;
    }

    /**
     * Refresh categories list
     * @param listView List view
     */
    private void refreshCategoriesList(ListView listView){
        CategoriesListRefresher refresher = new CategoriesListRefresher();
        refresher.execute(listView);
    }

    /**
     * Refresh replacement categories list
     * @param listView List view
     */
    private void refreshReplacementCategoriesList(ListView listView, int categoryId){
        listView.setAdapter(null);
        ReplacementCategoriesListRefresher refresher = new ReplacementCategoriesListRefresher();
        refresher.execute(listView, categoryId);
    }

    /**
     * Show edit box, hide list, set operation flag, fill fields, etc
     * @param adapter List view adapter
     * @param position Values list position
     */
    protected void showCategoryEditBox(CategoriesArrayAdapter adapter, int position) {
        TextView categoryEditText = (TextView) activity.findViewById(R.id.textViewCategoryEdit);
        EditText categoryNameEdit = (EditText) activity.findViewById(R.id.editTextCategoryName);
        LinearLayout categoryEditLayout = (LinearLayout) activity.findViewById(R.id.layoutCategoryEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutCategoriesList);
        LinearLayout replacementLayout = (LinearLayout) activity.findViewById(R.id.layoutSelectReplacement);
        ListView replacementList = (ListView) activity.findViewById(R.id.listViewReplacementCategory);
        RadioButton replaceYesButton = (RadioButton) activity.findViewById(R.id.radioButtonReplaceYes);
        RadioButton replaceNoButton = (RadioButton) activity.findViewById(R.id.radioButtonReplaceNo);

        layoutList.setVisibility(View.GONE);
        //adjust edit box to action
        switch (activity.budgetWiseAppState.categoryOperationFlag){
            case MainActivity.BudgetWiseAppState.CATEGORY_ADD:
                activity.budgetWiseAppState.currentCategoryId = 0;
                categoryEditText.setText(R.string.category_add);
                categoryNameEdit.setText("");
                //show/hide views for the mode
                categoryNameEdit.setVisibility(View.VISIBLE);
                replacementLayout.setVisibility(View.GONE);
                break;
            case MainActivity.BudgetWiseAppState.CATEGORY_EDIT:
                if ((adapter != null) && (position >= 0)) {
                    activity.budgetWiseAppState.currentCategoryId = adapter.values.get(position).id;
                    categoryEditText.setText(R.string.category_edit);
                    categoryNameEdit.setText(adapter.values.get(position).name);
                    //show/hide views for the mode
                    categoryNameEdit.setVisibility(View.VISIBLE);
                    replacementLayout.setVisibility(View.GONE);
                }
                break;
            case MainActivity.BudgetWiseAppState.CATEGORY_DELETE:
                activity.budgetWiseAppState.currentCategoryId = adapter.values.get(position).id;
                activity.budgetWiseAppState.replacementCategoryId = 0;
                categoryEditText.setText(R.string.category_delete_q);
                //show/hide views for the mode
                categoryNameEdit.setVisibility(View.GONE);
                //if category has links, show list of categories to replace it
                boolean inUse = false;
                try {
                    inUse = databaseHelper.checkCategoryUsage(activity.budgetWiseAppState.currentCategoryId);
                } catch (Exception e) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, "Error! Can't check Category usage", Toast.LENGTH_LONG).show();
                        }
                    });                }
                if (inUse) {
                    refreshReplacementCategoriesList(replacementList, activity.budgetWiseAppState.currentCategoryId);
                    replacementList.setVisibility(View.VISIBLE);
                    replaceYesButton.setChecked(true);
                    replaceNoButton.setChecked(false);
                    replacementLayout.setVisibility(View.VISIBLE);
                } else {
                    replaceYesButton.setChecked(false);
                    replaceNoButton.setChecked(true);
                    replacementLayout.setVisibility(View.GONE);
                }

                break;
        }
        //
        categoryEditLayout.setVisibility(View.VISIBLE);
        categoryNameEdit.requestFocus();
        //show keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(categoryNameEdit, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Hide edit box, show list, reset operation flag, hide keyboard
     * @param v Edit box view
     */
    protected void hideCategoryEditBox(View v) {
        activity.budgetWiseAppState.categoryOperationFlag = MainActivity.BudgetWiseAppState.CATEGORY_NO_OPERATION;

        LinearLayout categoryEditLayout = (LinearLayout) activity.findViewById(R.id.layoutCategoryEdit);
        LinearLayout layoutList = (LinearLayout) activity.findViewById(R.id.layoutCategoriesList);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        categoryEditLayout.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
    }
}
